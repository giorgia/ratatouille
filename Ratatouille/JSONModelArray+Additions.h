//
//  JSONModelArray+Additions.h
//  Ratatouille
//
//  Created by Giorgia Marenda on 10/19/17.
//  Copyright © 2017 Giorgia Marenda. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface JSONModelArray (Additions)
-(NSArray*)toArray;
@end
