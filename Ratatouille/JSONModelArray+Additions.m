//
//  JSONModelArray+Additions.m
//  Ratatouille
//
//  Created by Giorgia Marenda on 10/19/17.
//  Copyright © 2017 Giorgia Marenda. All rights reserved.
//

#import "JSONModelArray+Additions.h"

@implementation JSONModelArray (Additions)

- (NSArray*)toArray {
    NSMutableArray * array = [[NSMutableArray alloc] init];
    for (int i=0; i<self.count; i++) {
        [array addObject:[self objectAtIndex:i]];
    }
    return array;
}

@end
