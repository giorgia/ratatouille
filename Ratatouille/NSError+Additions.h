//
//  NSError+Additions.h
//  Ratatouille
//
//  Created by Giorgia Marenda on 12/30/13.
//  Copyright (c) 2013 Giorgia Marenda. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSError (Additions)

- (NSString*)errorDescription;
+ (NSString*)serverErrorDescriptionFrom:(NSDictionary*)error;

@end
