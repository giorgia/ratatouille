//
//  NSError+Additions.m
//  Ratatouille
//
//  Created by Giorgia Marenda on 12/30/13.
//  Copyright (c) 2013 Giorgia Marenda. All rights reserved.
//

#import "NSError+Additions.h"

@implementation NSError (Additions)

- (NSString*)errorDescription
{
    if ([self userInfo][JSONResponseSerializerWithDataKey])
        return [self userInfo][JSONResponseSerializerWithDataKey];
    return self.localizedDescription;
}


+ (NSString*)serverErrorDescriptionFrom:(NSDictionary*)error
{
    NSString *errorString = @"";
    
    if(error[@"error"] && error[@"status"])
    {
        errorString = [errorString stringByAppendingFormat:@"Error code %@\n %@\n", error[@"status"], error[@"error"]];
    }
    
    if (error[@"errors"] && [error[@"errors"] isKindOfClass:[NSDictionary class]])
    {
        for (NSString *key in [error[@"errors"] allKeys]) {
            errorString = [errorString stringByAppendingFormat:@"%@: %@\n", [key capitalizedString], [error[@"errors"][key] componentsJoinedByString:@","]];
        }
    }
    
    if (error[@"errors"] && [error[@"errors"] isKindOfClass:[NSArray class]])
    {
        errorString = [errorString stringByAppendingFormat:@"%@\n", [error[@"errors"] componentsJoinedByString:@","]];
        
    }
    
    return errorString;
}

@end
