//
//  NSString+MD5.h
//  Ratatouille
//
//  Created by Giorgia Marenda on 12/28/13.
//  Copyright (c) 2013 Giorgia Marenda. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (MD5)

- (NSString *)MD5;

@end
