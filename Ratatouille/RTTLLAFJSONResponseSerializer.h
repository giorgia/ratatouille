//
//  RTTLLAFJSONResponseSerializer.h
//  Ratatouille
//
//  Created by Giorgia Marenda on 12/28/13.
//  Copyright (c) 2013 Giorgia Marenda. All rights reserved.
//

#import "AFURLResponseSerialization.h"

@interface RTTLLAFJSONResponseSerializer : AFJSONResponseSerializer

@end
