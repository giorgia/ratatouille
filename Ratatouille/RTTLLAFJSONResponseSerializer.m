//
//  RTTLLAFJSONResponseSerializer.m
//  Ratatouille
//
//  Created by Giorgia Marenda on 12/28/13.
//  Copyright (c) 2013 Giorgia Marenda. All rights reserved.
//

#import "RTTLLAFJSONResponseSerializer.h"
#import "RTTLLSignUpViewController.h"
#import "RTTLLLoginViewController.h"
#import "RTTLLHTTPSessionManager.h"
#import "RTTLLAppDelegate.h"

@implementation RTTLLAFJSONResponseSerializer

- (id)responseObjectForResponse:(NSURLResponse *)response
                           data:(NSData *)data
                          error:(NSError *__autoreleasing *)error
{
    if (![self validateResponse:(NSHTTPURLResponse *)response data:data error:error]) {
        if (*error != nil) {
            
            NSError* jsonError;
            NSDictionary* json = [NSJSONSerialization
                                  JSONObjectWithData:data
                                  options:kNilOptions 
                                  error:&jsonError];

            if(!jsonError && [json count]) {
               
                NSMutableDictionary *userInfo = [(*error).userInfo mutableCopy];

                userInfo[JSONResponseSerializerWithDataKey] = [NSError serverErrorDescriptionFrom:json];
            
                NSError *newError = [NSError errorWithDomain:(*error).domain code:(*error).code userInfo:userInfo];
                (*error) = newError;
            }
        }
    
        return (nil);
    }
    
    return ([super responseObjectForResponse:response data:data error:error]);
}

@end
