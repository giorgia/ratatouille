//
//  RTTLLActionsViewController.h
//  Ratatouille
//
//  Created by Giorgia Marenda on 2/2/14.
//  Copyright (c) 2014 Giorgia Marenda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RTTLLItemViewController.h"

@class RTTLLItem;
@class RTTLLUser;

@protocol RTTLLItemActionsDelegate;

@interface RTTLLActionsViewController : UIViewController

@property (nonatomic, assign)       id <RTTLLItemActionsDelegate>  delegate;

@property (nonatomic, strong)       RTTLLUser *user;
@property (nonatomic, strong)       RTTLLItem *item;
@property (nonatomic, assign)       BOOL      isMine;

@end

@protocol RTTLLItemActionsDelegate <NSObject>

- (void)didDismissItemViewController:(RTTLLItemViewController*)itemController;

@end