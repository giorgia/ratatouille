//
//  RTTLLActionsViewController.m
//  Ratatouille
//
//  Created by Giorgia Marenda on 2/2/14.
//  Copyright (c) 2014 Giorgia Marenda. All rights reserved.
//
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>

#import "RTTLLActionsViewController.h"
#import "RTTLLItemViewController.h"
#import "RTTLLShareManager.h"
#import "UIView+Additions.h"
#import "PSPDFAlertView.h"
#import "RTTLLUser.h"
#import "RTTLLItem.h"

@interface RTTLLActionsViewController () <RTTLLItemDelegate, MFMessageComposeViewControllerDelegate, MFMailComposeViewControllerDelegate>

@property (strong, nonatomic)          NSURL            *imageURL;
@property (strong, nonatomic) IBOutlet UIView           *contactView;
@property (strong, nonatomic) IBOutlet UILabel          *userNameLabel;
@property (strong, nonatomic) IBOutlet UIButton         *phoneButton;
@property (strong, nonatomic) IBOutlet UIButton         *smsButton;
@property (strong, nonatomic) IBOutlet UIButton         *mailButton;

@property (strong, nonatomic) IBOutlet UIView           *actionsView;
@property (strong, nonatomic) IBOutlet UIButton         *takenButton;
@property (strong, nonatomic) IBOutlet UIButton         *editButton;
@property (strong, nonatomic) IBOutlet UIButton         *deleteButton;
@property (strong, nonatomic) IBOutlet UIButton         *shareButton;

@property (strong, nonatomic) IBOutlet UIView           *datePickerView;
@property (strong, nonatomic) IBOutlet UIDatePicker     *datePicker;

@property (strong, nonatomic) IBOutlet UIBarButtonItem  *actionButton;
@property (strong, nonatomic) IBOutlet UIBarButtonItem  *closeButton;
@property (strong, nonatomic) IBOutlet UIButton         *doneButton;
@property (strong, nonatomic) IBOutlet UIButton         *cancelButton;

@property (strong, nonatomic) RTTLLItemViewController   *itemController;

@end

@implementation RTTLLActionsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    RTTLLCategory *category = [RTTLLCategory categoryWithId:self.item.category_id];
    self.imageURL           = [NSURL URLWithString:category.image_url];
    
    [self.actionButton setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor whiteColor],
                                                NSFontAttributeName:LATO_BOLD(12)} forState:UIControlStateNormal];
    
    [self.closeButton setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor whiteColor],
                                               NSFontAttributeName:LATO_BOLD(12)} forState:UIControlStateNormal];
    
    [self.datePickerView topBorderOfWidth:4.0 andColor:COLOR_TORTORA];
    self.doneButton.titleLabel.font     = LATO_REG(17);
    self.cancelButton.titleLabel.font   = LATO_REG(17);
    self.datePickerView.frame           = CGRectMake(0.0, [UIScreen mainScreen].bounds.size.height,
                                                     self.datePickerView.frame.size.width,
                                                     self.datePickerView.frame.size.height);
    
    self.takenButton.layer.borderWidth  = 0.5;
    self.takenButton.layer.borderColor  = [UIColor whiteColor].CGColor;
    self.takenButton.titleLabel.font    = LATO_REG(12);
    
    if(!self.user) {
        self.user = self.item.user;
    }
    
    if (self.user) {
        self.userNameLabel.font = LATO_REG(12);
        self.userNameLabel.text = [NSString stringWithFormat:@"%@ %@", self.user.name, self.user.surname];
        self.phoneButton.hidden = !self.user.mobile.length;
        self.smsButton.hidden   = !self.user.mobile.length;
    }
    
    if(self.isMine) {

        if (!self.item) {
            self.title = @"ADD NEW ITEM";
            [self.actionButton setTitle:@"DONE"];
            
        } else {
            
            [self.actionButton setTitle:@""];
            [self.actionButton setImage:[UIImage imageNamed:@"item_arrow"]];
            self.actionsView.frame = CGRectMake(0.0, 0.0, self.contactView.frame.size.width, self.contactView.frame.size.height);
            [self.actionsView bottomBorderOfWidth:1 andColor:COLOR_LOBSTER];
        }
    } else {
        
        [self.actionButton setTitle:@"I WANT IT"];
        
        self.contactView.frame = CGRectMake(0.0, 0.0, self.contactView.frame.size.width, self.contactView.frame.size.height);
        [self.contactView bottomBorderOfWidth:1 andColor:COLOR_LOBSTER];
    }
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.destinationViewController isKindOfClass:[RTTLLItemViewController class]])
    {
        self.itemController             = (RTTLLItemViewController*)segue.destinationViewController;
        self.itemController.delegate    = self;
        self.itemController.isMine      = self.isMine;
        self.itemController.item        = self.item;
    }
}

- (void)toggleAction
{
    if(self.item) {
        [self.actionButton setImage:nil];
        [self.actionButton setTitle:@"DONE"];
        [self.itemController setEditble:YES];
    } else {
        [self.actionButton setTitle:@""];
        [self.actionButton setImage:[UIImage imageNamed:@"item_arrow"]];
        [self.itemController setEditble:NO];
    }
}

- (void)toggleMenu:(UIView*)menu
{
    [UIView animateWithDuration:0.3 animations:^{
        menu.frame = CGRectMake(0.0, menu.frame.origin.y?0.0:64.0, menu.frame.size.width, menu.frame.size.height);
    }];
}

- (void)toggleDatePicker
{
    CGFloat screenHeight    = [UIScreen mainScreen].bounds.size.height;
    CGFloat y               = self.datePickerView.frame.origin.y < screenHeight ? screenHeight : screenHeight - self.datePickerView.frame.size.height;
    
    [UIView animateWithDuration:0.3 animations:^{
        self.datePickerView.frame = CGRectMake(0.0, y,
                                               self.datePickerView.frame.size.width,
                                               self.datePickerView.frame.size.height);
    }];
}

#pragma mark - IBAction

- (IBAction)onAction:(id)sender
{
    if(self.isMine) {
        if (self.itemController.editMode) {
            [self.itemController donePressed];
            
        } else {
           [self toggleMenu:self.actionsView];
        }
    } else {
        //contact
        [self toggleMenu:self.contactView];
    }
}

- (IBAction)onEdit:(id)sender
{
    [self toggleMenu:self.actionsView];
    [self toggleAction];
}

- (IBAction)onDoneDate:(id)sender
{
    [self toggleDatePicker];
    if (self.datePicker.datePickerMode == UIDatePickerModeDate) {
         self.itemController.expirationDate = self.datePicker.date;
    } else {
        self.itemController.dueDate = self.datePicker.date;
    }
}

- (IBAction)onCancelDate:(id)sender
{
    [self toggleDatePicker];
}


- (IBAction)onDelete:(id)sender
{
    [self toggleMenu:self.actionsView];
    PSPDFAlertView *alert = [[PSPDFAlertView alloc] initWithTitle:@"Delete Item" message:@"Are you sure do you want to delete this item?"];
    
    [alert addButtonWithTitle:@"Yes" block:^{
        [self.itemController deleteItem];
    }];
    [alert addButtonWithTitle:@"Cancel" block:nil];
    
    [alert showWithTintColor:COLOR_LOBSTER];
}

- (IBAction)onTaken:(id)sender
{
    [self.itemController markAsTaken];
}

- (IBAction)onShare:(id)sender
{
    NSString *text  = @"I’m sharing this item on Ratatouille App! @Ratatouilleapp #ratatouille #foodsharing";
    UIImage *image  = [UIImage imageWithData:[NSData dataWithContentsOfURL:self.imageURL]];
    NSURL *url      = [NSURL URLWithString:ITEM_URL(self.item.id)];
    [RTTLLShareManager shareActionFrom:self withText:text image:image url:url];
}

- (IBAction)onCall:(id)sender
{
    UIDevice *device = [UIDevice currentDevice];
    
    if (![[device model] isEqualToString:@"iPhone"])
    {
        [SVProgressHUD showErrorWithStatus:@"Your device doesn't support calls!"];
        return;
    }
    
    PSPDFAlertView *alert = [[PSPDFAlertView alloc] initWithTitle:@"Call" message:[NSString stringWithFormat:@"Do you want to call %@?", self.user.name]];
    
    [alert addButtonWithTitle:@"Cancel"];
    [alert addButtonWithTitle:@"Yes" extendedBlock:^(PSPDFAlertView *alert, NSInteger buttonIndex)
    {
        NSString *phoneCall = [NSString stringWithFormat:@"tel:%@", self.user.mobile];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneCall]];

    }];
    [alert showWithTintColor:COLOR_BRICK];
}

- (IBAction)onMessage:(id)sender
{
    if(![MFMessageComposeViewController canSendText]) {
        [SVProgressHUD showErrorWithStatus:@"Your device doesn't support SMS!"];
        return;
    }
    [SVProgressHUD showWithStatus:@"Wait..."];
    NSString *message = [NSString stringWithFormat:@"Hi %@! I'd love to have your '%@'. Thanks for sharing it!", self.user.name, self.item.name];
    
    MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
    messageController.messageComposeDelegate = self;
  
    if (self.user.mobile.length) {
        [messageController setRecipients:@[self.user.mobile]];
    }
    [messageController setBody:message];
   
    [[messageController navigationBar] setTintColor:COLOR_LOBSTER];

    // Present message view controller on screen
    [self presentViewController:messageController animated:YES completion:^{
        [SVProgressHUD dismiss];
    }];
}

- (IBAction)onEmail:(id)sender
{
    if(![MFMailComposeViewController canSendMail]) {
        [SVProgressHUD showErrorWithStatus:@"Your device doesn't support e-mail!"];
        return;
    }
    [SVProgressHUD showWithStatus:@"Wait..."];

    NSString *message = [NSString stringWithFormat:@"Hi %@! I'd love to have your '%@'. Thanks for sharing it!", self.user.name, self.item.name];
    
    MFMailComposeViewController *mailViewController = [[MFMailComposeViewController alloc] init];
    mailViewController.mailComposeDelegate = self;

    [mailViewController setToRecipients:@[self.user.email]];
    [mailViewController setSubject:@"A Ratatouiller would like to pick up your extra food!"];
    [mailViewController setMessageBody:message isHTML:YES];
    NSData *imageData = [NSData dataWithContentsOfURL:self.imageURL];
    [mailViewController addAttachmentData:imageData mimeType:@"image/png" fileName:@"cat.png"];
    // Present message view controller on screen
    [[mailViewController navigationBar] setTintColor:COLOR_LOBSTER];
    [self presentViewController:mailViewController animated:YES completion:^{
        [SVProgressHUD dismiss];
    }];

}

- (IBAction)onClose:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Item Delegate

-(void)didUpdateItemViewController:(RTTLLItemViewController *)itemController
{
    if ([self.delegate respondsToSelector:@selector(didDismissItemViewController:)]) {
        [self.delegate didDismissItemViewController:itemController];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)itemViewController:(RTTLLItemViewController *)itemController showDatePickerWithDate:(NSDate *)date withType:(UIDatePickerMode)mode
{
    self.datePicker.datePickerMode = mode;
    if (date) {
        self.datePicker.date = date;
    }
    [self toggleDatePicker];
}

#pragma mark - Message Compose Delegate

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult) result
{
    switch (result) {
        case MessageComposeResultCancelled:
            break;
            
        case MessageComposeResultFailed:
        {
            [SVProgressHUD showErrorWithStatus:@"Failed to send SMS!"];
            break;
        }
            
        case MessageComposeResultSent:
        {
            [SVProgressHUD showSuccessWithStatus:@"SMS sent!"];
            break;
        }
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Mail Compose Delegate

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result) {
        case MFMailComposeResultCancelled:
            break;
            
        case MFMailComposeResultFailed:
        {
            [SVProgressHUD showErrorWithStatus:@"Failed to send email!"];
            break;
        }
            
        case MFMailComposeResultSent:
        {
            [SVProgressHUD showSuccessWithStatus:@"Email sent!"];
            break;
        }
        case MFMailComposeResultSaved:
        {
            [SVProgressHUD showSuccessWithStatus:@"Email saved!"];
            break;
        }
            
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
