//
//  RTTLLAmazonS3Client.h
//  Ratatouille
//
//  Created by Giorgia Marenda on 12/28/13.
//  Copyright (c) 2013 Giorgia Marenda. All rights reserved.
//

#import "AmazonS3Client.h"

@interface RTTLLAmazonS3Client : AmazonS3Client

+ (RTTLLAmazonS3Client *)sharedManager;
- (void)uploadImageWithName:(NSString*)imageName andImageData:(NSData*)data complention:(void (^)(NSString *resurceUrl, NSError *error) )completion;

@end
