//
//  RTTLLAmazonS3Client.m
//  Ratatouille
//
//  Created by Giorgia Marenda on 12/28/13.
//  Copyright (c) 2013 Giorgia Marenda. All rights reserved.
//

#import "RTTLLAmazonS3Client.h"

@interface RTTLLAmazonS3Client () <AmazonServiceRequestDelegate>

@property (copy) void (^complentionBlock)(NSString *resorceUrl, NSError *error);

@end

@implementation RTTLLAmazonS3Client

+ (RTTLLAmazonS3Client *)sharedManager {
    static RTTLLAmazonS3Client *_sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        _sharedManager = [[RTTLLAmazonS3Client alloc] initWithAccessKey:@"AKIAIRYTN2LUPJIZ63YQ" withSecretKey:@"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"];
        _sharedManager.timeout = 240;
    });
    
    return _sharedManager;
}


- (void)uploadImageWithName:(NSString*)imageName andImageData:(NSData*)data complention:(void (^)(NSString *resorceUrl, NSError *error) )completion
{
    self.complentionBlock = completion;
    
    NSString *bucketName = @"ratatouille";
    
    S3PutObjectRequest *objReq = [[S3PutObjectRequest alloc] initWithKey:[NSString stringWithFormat:@"avatars/%@", imageName] inBucket:bucketName];
    objReq.contentType = @"image/png";
    objReq.cannedACL = [S3CannedACL publicRead];
    
    objReq.data = data;
    objReq.delegate = self;
    objReq.contentLength = [data length];
    
    [self putObject:objReq];
}

#pragma mark -
#pragma mark AS3 Delegate

-(void)request:(AmazonServiceRequest *)request didSendData:(long long)bytesWritten totalBytesWritten:(long long)totalBytesWritten totalBytesExpectedToWrite:(long long)totalBytesExpectedToWrite
{
    CGFloat progress = (totalBytesWritten /(totalBytesExpectedToWrite * 1.0f));
    [SVProgressHUD showProgress:progress
                         status:@"Uploading..."
                       maskType:SVProgressHUDMaskTypeClear];
}

- (void)request:(AmazonServiceRequest *)request didCompleteWithResponse:(AmazonServiceResponse *)response
{
    self.complentionBlock([response.request.url absoluteString], nil);
}

- (void)request:(AmazonServiceRequest *)request didFailWithError:(NSError *)error
{
    self.complentionBlock(NO, error);
}


@end
