//
//  RTTLLAppDelegate.h
//  Ratatouille
//
//  Created by Giorgia Marenda on 10/31/13.
//  Copyright (c) 2013 Giorgia Marenda. All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString *const SCSessionStateChangedNotification;

@interface RTTLLAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

- (void)loadStoryboard;
- (void)loadStoryboardAnimated:(BOOL)animate;
- (void)loadInitialViewController:(id)controller;

@end
