//
//  RTTLLAppDelegate.m
//  Ratatouille
//
//  Created by Giorgia Marenda on 10/31/13.
//  Copyright (c) 2013 Giorgia Marenda. All rights reserved.
//
//#import <Crashlytics/Crashlytics.h>
#import "RTTLLAppDelegate.h"
#import "RTTLLActionsViewController.h"
#import "RTTLLLocationManager.h"
#import "RTTLLHTTPSessionManager.h"
#import "RTTLLNavigationController.h"
#import "SVProgressHUD.h"

@implementation RTTLLAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
//    [Crashlytics startWithAPIKey:@"a2e55e6bf5fc5f185961737313fe0ee3fd465e5e"];
    // Override point for customization after application launch.
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor = COLOR_LIGHT_BEIGE;
    [self loadStoryboard];
    [self setupApparance];
    
    [self.window makeKeyAndVisible];
    
    return YES;
}

- (void)loadInitialViewController:(id)controller
{
    self.window.rootViewController = controller;
}

- (void)loadStoryboard
{
    UIStoryboard* storyboard = nil;
    UIViewController* initialViewController = nil;
    
    if ([RTTLLHTTPSessionManager userToken].length) {
        storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        initialViewController = [storyboard instantiateInitialViewController];
    } else {
        storyboard = [UIStoryboard storyboardWithName:@"Login" bundle:nil];
        initialViewController = [storyboard instantiateInitialViewController];
    }
    self.window.rootViewController = initialViewController;
}

- (void)loadStoryboardAnimated:(BOOL)animate
{
    if (animate) {
        [UIView animateWithDuration:0.5 animations:^{
            self.window.rootViewController.view.alpha = 0;
        } completion:^(BOOL finished) {
            [self loadStoryboard];
            if (finished) {
                [UIView animateWithDuration:0.5 animations:^{
                    self.window.rootViewController.view.alpha = 1;
                }];
            }
        }];
    } else {
        [self loadStoryboard];
    }
}

- (void)setupApparance
{
    //    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    self.window.backgroundColor = COLOR_BEIGE;
    
    [SVProgressHUD setFont:LATO_REG(13)];
    [SVProgressHUD appearance].tintColor = COLOR_LIGHT_LOBSTER;
    [SVProgressHUD setBackgroundColor:COLOR_TORTORA];
    [SVProgressHUD setForegroundColor:COLOR_LOBSTER];
    
    //labels
    [[UITextField appearanceWhenContainedInInstancesOfClasses:@[[UITableViewCell class]]] setFont:LATO_LIGHT(20)];
    [[UITextField appearanceWhenContainedInInstancesOfClasses:@[[UISearchBar class]]] setFont:LATO_LIGHT(14)];
    
    //UIToolbar
    [UIToolbar appearance].tintColor    = COLOR_LOBSTER;
    [UIToolbar appearance].barTintColor = COLOR_LIGHT_BEIGE;
    
    [[UIActionSheet appearance] setTintColor:COLOR_LOBSTER];
    
    //UINavigationBar
    [UINavigationBar appearanceWhenContainedInInstancesOfClasses:@[[RTTLLNavigationController class]]].barTintColor = COLOR_LOBSTER;
    [UINavigationBar appearanceWhenContainedInInstancesOfClasses:@[[RTTLLNavigationController class]]].titleTextAttributes = @{NSFontAttributeName:LATO_REG(17)};
    
    
    [[UIBarButtonItem appearance] setTitleTextAttributes:@{NSFontAttributeName:LATO_BOLD(12) }
                                                forState:UIControlStateNormal];
}


- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    //    [[RTTLLLocationManager locationManager] startMonitoringSignificantLocationChanges];
    
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    //    [[RTTLLLocationManager locationManager] stopMonitoringSignificantLocationChanges];
    //    [[RTTLLLocationManager locationManager] startUpdatingLocation];
    [FBSDKAppEvents activateApp];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification {
    NSLog(@"Notification fired");
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    BOOL urlWasHandled = [[FBSDKApplicationDelegate sharedInstance] application:application openURL:url sourceApplication:sourceApplication annotation:annotation];
    
    if (!urlWasHandled) {
        NSString *host = [url host];
        NSString *path = [url path];
        
        if ([host isEqualToString:ITEM_HOST]) {
            NSString *itemId = [path stringByReplacingOccurrencesOfString:@"/" withString:@""];
            
            [SVProgressHUD showWithStatus:@"Loading..."];
            
            [[RTTLLHTTPSessionManager sharedManager] itemWithId:[itemId integerValue] completion:^(RTTLLItem *item, NSError *error)
            {
                [SVProgressHUD dismiss];
                UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                
                RTTLLNavigationController *nav = [storyboard instantiateViewControllerWithIdentifier:@"item"];
                RTTLLActionsViewController *container = (RTTLLActionsViewController*)nav.visibleViewController;
                container.item = item;
                container.user = item.user;
                container.isMine = [item.user isMe];
                [self.window.rootViewController presentViewController:nav animated:YES completion:nil];
            }];
        }
        urlWasHandled = YES;
    } else {
        
        [SVProgressHUD showErrorWithStatus:@"Login!"];
    }
    
    return urlWasHandled;
}

@end
