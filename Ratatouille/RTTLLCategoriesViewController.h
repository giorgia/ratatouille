//
//  RTTLLCategoriesViewController.h
//  Ratatouille
//
//  Created by Giorgia Marenda on 1/10/14.
//  Copyright (c) 2014 Giorgia Marenda. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RTTLLCategory;

@protocol RTTLLCategoryChooserDelegate;

@interface RTTLLCategoriesViewController : UITableViewController

@property (nonatomic, assign) id <RTTLLCategoryChooserDelegate>  delegate;

@end

@protocol RTTLLCategoryChooserDelegate <NSObject>

- (void)dismissChooserWithSelectedCategory:(RTTLLCategory*)category;

@end