//
//  RTTLLCategoriesViewController.m
//  Ratatouille
//
//  Created by Giorgia Marenda on 1/10/14.
//  Copyright (c) 2014 Giorgia Marenda. All rights reserved.
//

#import "RTTLLCategoriesViewController.h"
#import "UIImageView+AFNetworking.h"
#import "RTTLLHTTPSessionManager.h"
#import "RTTLLCategory.h"

@interface RTTLLCategoriesViewController () <UITableViewDelegate>

@property (nonatomic,strong) NSArray *categories;

@end

@implementation RTTLLCategoriesViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    __block typeof(self) bself = self;
    [[RTTLLHTTPSessionManager sharedManager] categoriesCompletion:^(NSArray *results, NSError *error) {
        bself.categories = results;
        [self.tableView reloadData];
    }];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.categories count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];

    if(!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    cell.backgroundColor                = [UIColor clearColor];
    cell.textLabel.font                 = LATO_REG(16);
    cell.textLabel.textColor            = COLOR_SMERALD;
    cell.textLabel.numberOfLines        = 0;
    cell.detailTextLabel.font           = LATO_LIGHT(12);
    cell.detailTextLabel.textColor      = COLOR_BROWN;
    cell.detailTextLabel.numberOfLines  = 0;
    
    cell.imageView.contentMode = UIViewContentModeScaleAspectFill;

    RTTLLCategory *cat = self.categories[indexPath.row];
    cell.textLabel.text = [[cat name] uppercaseString];
    [cell.imageView setImageWithURL:[NSURL URLWithString:cat.image_url] placeholderImage:[UIImage imageNamed:@"empty_category_fridge"]];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.delegate respondsToSelector:@selector(dismissChooserWithSelectedCategory:)]) {
        [self.delegate dismissChooserWithSelectedCategory:[self.categories objectAtIndex:indexPath.row]];
    }
    [self.navigationController popViewControllerAnimated:YES];
}

@end
