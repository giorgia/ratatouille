//
//  RTTLLCategory.h
//  Ratatouille
//
//  Created by Giorgia Marenda on 11/3/13.
//  Copyright (c) 2013 Giorgia Marenda. All rights reserved.
//

#import "JSONModel.h"

@protocol RTTLLCategory
@end

@interface RTTLLCategory : JSONModel

@property (assign, nonatomic) int                         id;
@property (strong, nonatomic) NSString<Optional>          *name;
@property (strong, nonatomic) NSString<Optional>          *image_url;

+ (void)categoryWithId:(NSString*)categoryId complention:(void (^) (RTTLLCategory* category, NSError *error)) complention;
+ (RTTLLCategory*)categoryWithId:(NSString*)categoryId;

@end
