//
//  RTTLLCategory.m
//  Ratatouille
//
//  Created by Giorgia Marenda on 11/3/13.
//  Copyright (c) 2013 Giorgia Marenda. All rights reserved.
//

#import "RTTLLCategory.h"
#import "RTTLLHTTPSessionManager.h"

@implementation RTTLLCategory

+ (void)categoryWithId:(NSString*)categoryId complention:(void (^) (RTTLLCategory* category, NSError *error)) complention
{
    [[RTTLLHTTPSessionManager sharedManager] categoriesCompletion:^(NSArray *results, NSError *error) {
        if (error) {
            complention(nil, error);
        }
        NSArray *filteredCats = [results filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"id == %d", [categoryId intValue]]];
        if ([filteredCats count]) {
            complention([filteredCats lastObject], nil);
        }
        complention(nil, nil);
    }];
}

+ (RTTLLCategory*)categoryWithId:(NSString*)categoryId
{
    NSArray *categories = [[RTTLLHTTPSessionManager sharedManager].cacheData objectForKey:@"categories"];
    if ([categories count]) {
        NSArray *filteredCats = [categories filteredArrayUsingPredicate:
                                 [NSPredicate predicateWithFormat:@"id == %d", [categoryId intValue]]];
        if ([filteredCats count]) {
            return [filteredCats lastObject];
        }
    }
    return nil;
}

@end
