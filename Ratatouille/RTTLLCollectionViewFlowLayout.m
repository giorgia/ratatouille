//
//  RTTLLCollectionViewFlowLayout.m
//  Ratatouille
//
//  Created by Giorgia Marenda on 11/2/13.
//  Copyright (c) 2013 Giorgia Marenda. All rights reserved.
//

#import "RTTLLCollectionViewFlowLayout.h"
#import "RTTLLRowView.h"

@interface RTTLLCollectionViewFlowLayout ()

@property (nonatomic, strong) UIDynamicAnimator *dynamicAnimator;

@property (nonatomic, strong) NSMutableSet      *visibleIndexPathsSet;
@property (nonatomic, assign) CGFloat           latestDelta;
@property (nonatomic, strong) NSDictionary      *shelfRects;

@end

@implementation RTTLLCollectionViewFlowLayout

-(id)init {
    if (!(self = [super init])) return nil;
    
    return self;
}

-(void)awakeFromNib
{
    self.minimumInteritemSpacing    = 0;
    self.minimumLineSpacing         = 20;
    self.itemSize                   = CGSizeMake(100, 158);
    self.sectionInset               = UIEdgeInsetsMake(0, 10, 10, 10);

    self.dynamicAnimator = [[UIDynamicAnimator alloc] initWithCollectionViewLayout:self];
    self.visibleIndexPathsSet = [NSMutableSet set];
    [self registerClass:[RTTLLRowView class] forDecorationViewOfKind:[RTTLLRowView kind]];

}

-(void)prepareLayout {
    [super prepareLayout];
    
    // Need to overflow our actual visible rect slightly to avoid flickering.
    CGRect visibleRect = CGRectInset((CGRect){.origin = self.collectionView.bounds.origin, .size = self.collectionView.frame.size}, -100, -100);
    
    NSArray *itemsInVisibleRectArray = [super layoutAttributesForElementsInRect:visibleRect];
    
    NSSet *itemsIndexPathsInVisibleRectSet = [NSSet setWithArray:[itemsInVisibleRectArray valueForKey:@"indexPath"]];
    
    // Step 1: Remove any behaviours that are no longer visible.
    NSArray *noLongerVisibleBehaviours = [self.dynamicAnimator.behaviors filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(UIAttachmentBehavior *behaviour, NSDictionary *bindings) {
//        BOOL currentlyVisible = [itemsIndexPathsInVisibleRectSet member:[[[behaviour items] firstObject] indexPath]] != nil;
//        return !currentlyVisible;
        return YES;
    }]];
    
    [noLongerVisibleBehaviours enumerateObjectsUsingBlock:^(id obj, NSUInteger index, BOOL *stop) {
        [self.dynamicAnimator removeBehavior:obj];
//        [self.visibleIndexPathsSet removeObject:[[[obj items] firstObject] indexPath]];
    }];
    
    // Step 2: Add any newly visible behaviours.
    // A "newly visible" item is one that is in the itemsInVisibleRect(Set|Array) but not in the visibleIndexPathsSet
    NSArray *newlyVisibleItems = [itemsInVisibleRectArray filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(UICollectionViewLayoutAttributes *item, NSDictionary *bindings) {
        BOOL currentlyVisible = [self.visibleIndexPathsSet member:item.indexPath] != nil;
        return !currentlyVisible;
    }]];
    
    CGPoint touchLocation = [self.collectionView.panGestureRecognizer locationInView:self.collectionView];
    
    [newlyVisibleItems enumerateObjectsUsingBlock:^(UICollectionViewLayoutAttributes *item, NSUInteger idx, BOOL *stop) {
        CGPoint center                          = item.center;
        UIAttachmentBehavior *springBehaviour   = [[UIAttachmentBehavior alloc] initWithItem:item attachedToAnchor:center];
        
        springBehaviour.length      = 0.5f;
        springBehaviour.damping     = 0.3f;
        springBehaviour.frequency   = 1.5f;
        
        // If our touchLocation is not (0,0), we'll need to adjust our item's center "in flight"
        if (!CGPointEqualToPoint(CGPointZero, touchLocation)) {
            CGFloat yDistanceFromTouch  = fabs(touchLocation.y - springBehaviour.anchorPoint.y);
            CGFloat xDistanceFromTouch  = fabs(touchLocation.x - springBehaviour.anchorPoint.x);
            CGFloat scrollResistance    = (yDistanceFromTouch + xDistanceFromTouch) / 1500.0f;
            
            if (self.latestDelta < 0) {
                center.y += MAX(self.latestDelta, self.latestDelta*scrollResistance);
            }
            else {
                center.y += MIN(self.latestDelta, self.latestDelta*scrollResistance);
            }
            item.center = center;
        }
        
        [self.dynamicAnimator addBehavior:springBehaviour];
        [self.visibleIndexPathsSet addObject:item.indexPath];
    }];
    
    // Calculate where shelves go in a horizontal layout
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];

    CGFloat y                       = self.sectionInset.top;
    CGFloat availableHeight         = self.collectionViewContentSize.height - (self.sectionInset.top + self.sectionInset.bottom);
    int itemsAcross                 = floorf((availableHeight + self.minimumInteritemSpacing) / (self.itemSize.height + self.minimumInteritemSpacing));

    for (int row = 0; row < itemsAcross; row++)
    {
        y += self.itemSize.height + self.minimumLineSpacing / 2;
        dictionary[[NSIndexPath indexPathForItem:row inSection:0]] = [NSValue valueWithCGRect:CGRectMake(self.sectionInset.left, y, self.collectionViewContentSize.width - self.minimumLineSpacing, 2)];
        y += self.minimumLineSpacing / 2;

    }
    self.shelfRects = [NSDictionary dictionaryWithDictionary:dictionary];

}

// layout attributes for a specific decoration view
- (UICollectionViewLayoutAttributes *)layoutAttributesForDecorationViewOfKind:(NSString *)decorationViewKind atIndexPath:(NSIndexPath *)indexPath
{
    id shelfRect = self.shelfRects[indexPath];
    
    UICollectionViewLayoutAttributes *attributes = [UICollectionViewLayoutAttributes layoutAttributesForDecorationViewOfKind:[RTTLLRowView kind] withIndexPath:indexPath];
    attributes.frame    = [shelfRect CGRectValue];
    attributes.zIndex   = 0;
    
    return attributes;
}

-(NSArray *)layoutAttributesForElementsInRect:(CGRect)rect {
    
    NSArray *array = [self.dynamicAnimator itemsInRect:rect];
    
    // Add our decoration views (shelves)
    NSMutableArray *newArray = [array mutableCopy];
    
    [self.shelfRects enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {

            UICollectionViewLayoutAttributes *attributes = [UICollectionViewLayoutAttributes layoutAttributesForDecorationViewOfKind:[RTTLLRowView kind] withIndexPath:key];
            attributes.frame = [obj CGRectValue];
            attributes.zIndex = 0;
            //attributes.alpha = 0.5; // screenshots
            [newArray addObject:attributes];
    }];
    
    array = [NSArray arrayWithArray:newArray];
    
    return array;
}

-(UICollectionViewLayoutAttributes *)layoutAttributesForItemAtIndexPath:(NSIndexPath *)indexPath {
    return [self.dynamicAnimator layoutAttributesForCellAtIndexPath:indexPath];
}

-(BOOL)shouldInvalidateLayoutForBoundsChange:(CGRect)newBounds {
    UIScrollView *scrollView    = self.collectionView;
    CGFloat delta               = newBounds.origin.y - scrollView.bounds.origin.y;
    self.latestDelta            = delta;
    CGPoint touchLocation       = [self.collectionView.panGestureRecognizer locationInView:self.collectionView];
    
    [self.dynamicAnimator.behaviors enumerateObjectsUsingBlock:^(UIAttachmentBehavior *springBehaviour, NSUInteger idx, BOOL *stop) {
        CGFloat yDistanceFromTouch  = fabsf(touchLocation.y - springBehaviour.anchorPoint.y);
        CGFloat xDistanceFromTouch  = fabsf(touchLocation.x - springBehaviour.anchorPoint.x);
        CGFloat scrollResistance    = (yDistanceFromTouch + xDistanceFromTouch) / 1500.0f;
        
        UICollectionViewLayoutAttributes *item = [springBehaviour.items firstObject];
        CGPoint center = item.center;
        if (delta < 0) {
            center.y += MAX(delta, delta*scrollResistance);
        }
        else {
            center.y += MIN(delta, delta*scrollResistance);
        }
        item.center = center;
        
        [self.dynamicAnimator updateItemUsingCurrentState:item];
    }];
    
    return NO;
}


@end
