//
//  RTTLLConstants.h
//  Ratatouille
//
//  Created by Giorgia Marenda on 12/7/13.
//  Copyright (c) 2013 Giorgia Marenda. All rights reserved.
//

#ifndef Ratatouille_RTTLLConstants_h
#define Ratatouille_RTTLLConstants_h

static NSString * const JSONResponseSerializerWithDataKey = @"JSONResponseSerializerWithDataKey";

typedef enum {
    RTTLLShareTypeFacebook = 0, 
    RTTLLShareTypeTwitter
} RTTLLShareType;

//#define BASE_URL @"http://ratatouille.apiary.io"

#define BASE_URL                @"https://ratatouille-app.herokuapp.com/"

#define FORGOT_PWD_URL          [NSString stringWithFormat:@"%@/users/password/new", BASE_URL]
#define ITEM_HOST               @"items.ratatouille-app.com"
#define ITEM_URL(_ID_)          [NSString stringWithFormat:@"rttll://%@/%d", ITEM_HOST, _ID_]

#define PRIVACY_URL             [NSString stringWithFormat:@"%@privacy", BASE_URL]
#define TERM_OF_SERVICE_URL     [NSString stringWithFormat:@"%@tos", BASE_URL]

#define APP_STORE_ID            @"847404887"
#define APP_STORE_LINK          [NSString stringWithFormat:@"https://itunes.apple.com/us/app/ratatouille/id%@", APP_STORE_ID]

#define SHARE_TEXT              @"Ratatouille is the best app to share your extra food.#ratatouille #foodsharing"


#define SIGNIN                  @"signin"
#define SIGNUP                  @"signup"
#define SIGNOUT                 @"signout"
#define ME                      @"me"
#define USERS                   @"users"
#define FRIDGE                  @"fridge"
#define CATEGORIES              @"categories"
#define STATS                   @"stats"
#define ITEMS                   @"items"

#define USERS_WITH_ID(_ID_)     [NSString stringWithFormat:@"users/%@", _ID_]
#define FRIDGES_WITH_ID(_ID_)   [NSString stringWithFormat:@"fridges/%@", _ID_]
#define ITEMS_WITH_ID(_ID_)     [NSString stringWithFormat:@"items/%@", _ID_]


#define LATO_REG(_size_)        [UIFont fontWithName:@"Lato-Regular" size:_size_]
#define LATO_LIGHT(_size_)      [UIFont fontWithName:@"Lato-Light" size:_size_]
#define LATO_BOLD(_size_)       [UIFont fontWithName:@"Lato-Bold" size:_size_]
#define LATO_BLACK(_size_)      [UIFont fontWithName:@"Lato-Black" size:_size_]
#define CASSANNET_BOLD(_size_)  [UIFont fontWithName:@"Cassannet-Bold" size:_size_]

#define COLOR_LOBSTER           [UIColor colorWithRed:219.0/255.0 green:86.0/255.0 blue:47.0/255.0 alpha:1.0]
#define COLOR_LIGHT_LOBSTER     [UIColor colorWithRed:229.0/255.0 green:115.0/255.0 blue:82.0/255.0 alpha:1.0]
#define COLOR_BROWN             [UIColor colorWithRed:123.0/255.0 green:118.0/255.0 blue:87.0/255.0 alpha:1.0]
#define COLOR_TORTORA           [UIColor colorWithRed:205.0/255.0 green:203.0/255.0 blue:182.0/255.0 alpha:1.0]
#define COLOR_OLIVE             [UIColor colorWithRed:143.0/255.0 green:166.0/255.0 blue:144.0/255.0 alpha:1.0]
#define COLOR_BEIGE             [UIColor colorWithRed:222.0/255.0 green:219.0/255.0 blue:201.0/255.0 alpha:1.0]
#define COLOR_LIGHT_BEIGE       [UIColor colorWithRed:241.0/255.0 green:240.0/255.0 blue:233.0/255.0 alpha:1.0]
#define COLOR_SMERALD           [UIColor colorWithRed:58.0/255.0 green:156.0/255.0 blue:155.0/255.0 alpha:1.0]
#define COLOR_BRICK             [UIColor colorWithRed:175.0/255.0 green:71.0/255.0 blue:16.0/255.0 alpha:1.0]
#define COLOR_ACID              [UIColor colorWithRed:191.0/255.0 green:189.0/255.0 blue:0.0/255.0 alpha:1.0]

#endif
