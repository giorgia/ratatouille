//
//  RTTLLFridge.h
//  Ratatouille
//
//  Created by Giorgia Marenda on 11/3/13.
//  Copyright (c) 2013 Giorgia Marenda. All rights reserved.
//

#import "JSONModel.h"
#import "RTTLLItem.h"
#import "RTTLLUser.h"

@interface RTTLLFridge : JSONModel

@property (assign, nonatomic) int                             id;
@property (strong, nonatomic) NSString<Optional>              *lat;
@property (strong, nonatomic) NSString<Optional>              *lng;
@property (strong, nonatomic) NSString<Optional>              *distance;
@property (strong, nonatomic) NSString<Optional>              *items_count;
@property (strong, nonatomic) NSArray<Optional, RTTLLItem>    *items;
@property (strong, nonatomic) RTTLLUser<Optional>             *user;

- (NSArray*)takenItems;
- (NSArray*)expiredItems;
- (NSArray*)validItems;

@end
