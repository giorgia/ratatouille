//
//  RTTLLFridge.m
//  Ratatouille
//
//  Created by Giorgia Marenda on 11/3/13.
//  Copyright (c) 2013 Giorgia Marenda. All rights reserved.
//

#import "RTTLLFridge.h"

@implementation RTTLLFridge

-(NSString<Optional> *)distance
{
    if(_distance && _distance.length) {
        CGFloat dist = [_distance integerValue];
        if(dist < 1000) {
            return [NSString stringWithFormat:@"%.0f M", dist];
        }
        dist = dist / 1000;
        return [NSString stringWithFormat:@"%.01f KM", dist];
    }
    
    return @"";
}

- (NSArray*)takenItems
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"is_taken == YES"];
    
    return [self.items filteredArrayUsingPredicate:predicate];
}

- (NSArray*)expiredItems
{
    NSDateFormatter *expformatter = [[NSDateFormatter alloc] init];
    [expformatter setDateFormat:@"yyyy-MM-dd'T'hh:mm:ss'Z'"];

    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"expiration_date < %@", [expformatter stringFromDate:[NSDate date]]];
    
    return [self.items filteredArrayUsingPredicate:predicate];
}

- (NSArray*)validItems
{
    NSDateFormatter *expformatter = [[NSDateFormatter alloc] init];
    [expformatter setDateFormat:@"yyyy-MM-dd'T'hh:mm:ss'Z'"];

    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"is_taken == NO AND expiration_date >= %@", [expformatter stringFromDate:[NSDate date]]]; //expire date
    
    return [self.items filteredArrayUsingPredicate:predicate];
}

@end
