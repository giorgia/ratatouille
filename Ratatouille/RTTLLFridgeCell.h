//
//  RTTLLFridgeCell.h
//  Ratatouille
//
//  Created by Giorgia Marenda on 12/8/13.
//  Copyright (c) 2013 Giorgia Marenda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RTTLLFridgeCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView  *avatarImageView;
@property (strong, nonatomic) IBOutlet UILabel      *nameLabel;
@property (strong, nonatomic) IBOutlet UILabel      *itemsLabel;
@property (strong, nonatomic) IBOutlet UILabel      *distanceLabel;

- (void)setImageFromUrl:(NSString*)URLString;

@end
