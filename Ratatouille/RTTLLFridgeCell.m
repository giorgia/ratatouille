//
//  RTTLLFridgeCell.m
//  Ratatouille
//
//  Created by Giorgia Marenda on 12/8/13.
//  Copyright (c) 2013 Giorgia Marenda. All rights reserved.
//

#import "RTTLLFridgeCell.h"
#import "UIImageView+AFNetworking.h"

@implementation RTTLLFridgeCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void)prepareForReuse
{
    [super prepareForReuse];
    self.avatarImageView.image = nil;
}

-(void)awakeFromNib
{
    [super awakeFromNib];
    self.avatarImageView.layer.cornerRadius = self.avatarImageView.frame.size.height / 2;
    self.nameLabel.font     = LATO_REG(15);
    self.itemsLabel.font    = LATO_LIGHT(12);
    self.distanceLabel.font = LATO_LIGHT(13);
}

- (void)setImageFromUrl:(NSString*)URLString
{
    __block typeof(self) bself = self;
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:URLString]];
    
    [self.avatarImageView setImageWithURLRequest:request placeholderImage:nil success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
     {
         bself.avatarImageView.alpha = 0.0;
         bself.avatarImageView.image = image;

         [UIView animateWithDuration:1.3 animations:^{
             bself.avatarImageView.alpha = 1.0;
         }];
         
     } failure:nil];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
