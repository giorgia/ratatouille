//
//  RTTLLFridgeLocationViewController.h
//  Ratatouille
//
//  Created by Giorgia Marenda on 1/7/14.
//  Copyright (c) 2014 Giorgia Marenda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface RTTLLFridgeLocationViewController : UITableViewController

@property (strong, nonatomic) CLLocation *location;

@end
