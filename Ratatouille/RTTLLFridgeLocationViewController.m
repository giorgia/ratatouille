//
//  RTTLLFridgeLocationViewController.m
//  Ratatouille
//
//  Created by Giorgia Marenda on 1/7/14.
//  Copyright (c) 2014 Giorgia Marenda. All rights reserved.
//
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import <AddressBookUI/AddressBookUI.h>

#import "RTTLLFridgeLocationViewController.h"
#import "RTTLLHTTPSessionManager.h"
#import "RTTLLAppDelegate.h"
#import "RTTLLPin.h"

@interface RTTLLFridgeLocationViewController ()<CLLocationManagerDelegate, UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet MKMapView    *mapView;
@property (strong, nonatomic) IBOutlet UIButton     *positionButton;
@property (strong, nonatomic) IBOutlet UITextField  *addressTextField;
@property (strong, nonatomic) IBOutlet UIButton     *saveButton;
@property (strong, nonatomic) CLLocationManager     *locationManager;
@property (strong, nonatomic) RTTLLPin              *annotation;

@end

@implementation RTTLLFridgeLocationViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.saveButton.titleLabel.font = LATO_REG(13);
    
    if(!self.location) {
        self.navigationItem.hidesBackButton = YES;
    }
    
    self.locationManager                    = [[CLLocationManager alloc] init];
    self.locationManager.desiredAccuracy    = kCLLocationAccuracyBest;
    self.locationManager.delegate           = self;
    [self.locationManager requestWhenInUseAuthorization];
    [self.locationManager startUpdatingLocation];
   
    self.annotation = [[RTTLLPin alloc] init];
    if (self.location) {
        [self updateLocation];
    }
}

- (IBAction)savePositionTapped:(UIButton *)sender
{
    if(self.location)
    {
        [SVProgressHUD showWithStatus:@"Sending location..."];
        [[RTTLLHTTPSessionManager sharedManager] updateFridgeLocation:self.location.coordinate completion:^(NSError *error) {
            [SVProgressHUD dismiss];
            if (error) {
                [SVProgressHUD showErrorWithStatus:[error errorDescription]];
                return;
            }
            RTTLLAppDelegate *delegate = (RTTLLAppDelegate *)[[UIApplication sharedApplication] delegate];
            [delegate loadStoryboardAnimated:YES];
        }];
    } else {
        [SVProgressHUD showErrorWithStatus:@"No location."];
    }
}

- (BOOL)checkAutorization
{
    if([CLLocationManager authorizationStatus]==kCLAuthorizationStatusDenied){
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"App Permission Denied"
                                                        message:@"To re-enable, please go to Settings and turn on Location Service for this app."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
      
                              [alert show];
        return NO;
    }
    return YES;
}

- (IBAction)searchBox:(UITextField *)sender
{
        CLGeocoder *geocoder = [[CLGeocoder alloc] init];
        [geocoder geocodeAddressString:sender.text completionHandler:^(NSArray *placemarks, NSError *error) {
            [SVProgressHUD showWithStatus:@"Finding location..."];
            
            if (error) {
                [SVProgressHUD showErrorWithStatus:error.description];
                NSLog(@"%@", error);
            } else {
                [SVProgressHUD dismiss];
                CLPlacemark *placemark = [placemarks lastObject];
                self.location = placemark.location;
                [self updateLocation];
            }
        }];
}

- (IBAction)myLocation:(UIButton *)sender
{
    if ([self checkAutorization]) {
        self.location = self.locationManager.location;
        [self updateLocation];
        [self reverseGeocode:self.location];
    }
}

- (void)updateLocation
{
    float spanX = 0.00001;
    float spanY = 0.00001;

    [self.mapView removeAnnotation:self.annotation];
    
    self.annotation.coordinate = self.location.coordinate;

    [self.mapView addAnnotation:self.annotation];
    
    MKCoordinateRegion region;
    region.center.latitude  = self.location.coordinate.latitude;
    region.center.longitude = self.location.coordinate.longitude;
    region.span = MKCoordinateSpanMake(spanX, spanY);
    [self.mapView setRegion:region animated:YES];
}

- (void)reverseGeocode:(CLLocation *)location
{
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error) {
        [SVProgressHUD showWithStatus:@"Finding address..."];
        if (error) {
            [SVProgressHUD showErrorWithStatus:error.description];
            NSLog(@"Error %@", error.description);
        } else {
            [SVProgressHUD dismiss];
            CLPlacemark *placemark = [placemarks lastObject];
            self.addressTextField.text = [NSString stringWithFormat:@"%@",ABCreateStringWithAddressDictionary(placemark.addressDictionary, NO)];
        }
    }];
}

#pragma mark -
#pragma mark Map View Delegate

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation {
    static NSString *viewIdentifier     = @"annotationView";
    MKPinAnnotationView *annotationView = (MKPinAnnotationView *) [mapView dequeueReusableAnnotationViewWithIdentifier:viewIdentifier];
    
    if (![annotation isKindOfClass:[MKUserLocation class]] && annotationView == nil) {
    	annotationView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:viewIdentifier];
        annotationView.annotation = annotation;
    }
    annotationView.image = [UIImage imageNamed:@"pin"];
    return annotationView;
}

#pragma mark -
#pragma mark Text View Delegate

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self searchBox:textField];
    [textField resignFirstResponder];
    return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
