//
//  RTTLLFridgeViewController.h
//  Ratatouille
//
//  Created by Giorgia Marenda on 11/1/13.
//  Copyright (c) 2013 Giorgia Marenda. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RTTLLUser;

@interface RTTLLFridgeViewController : UIViewController

@property (strong, nonatomic) IBOutlet RTTLLUser *user;

@end
