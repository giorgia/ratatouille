//
//  RTTLLFridgeViewController.m
//  Ratatouille
//
//  Created by Giorgia Marenda on 11/1/13.
//  Copyright (c) 2013 Giorgia Marenda. All rights reserved.
//


#import "RTTLLSettingsViewController.h"
#import "RTTLLActionsViewController.h"
#import "RTTLLFridgeViewController.h"
#import "UIImageView+AFNetworking.h"
#import "RTTLLHTTPSessionManager.h"
#import "RTTLLItemViewController.h"
#import "RTTLLShareManager.h"
#import "RTTLLItemViewCell.h"
#import "RTTLLCategory.h"
#import "RTTLLFridge.h"
#import "RTTLLItem.h"
#import "RTTLLUser.h"

#define EMPTY_ME_FRIGE_MSG @"YOUR FRIDGE IS EMPTY. WHY DON'T YOU ADD SOMETHING?"
#define EMPTY_FRIGE_MSG(_USER_) [NSString stringWithFormat:@"%@'S FRIDGE IS EMPTY.",[_USER_ uppercaseString]]

static NSString * CellIdentifier = @"CellIdentifier";

@interface RTTLLFridgeViewController () <UICollectionViewDelegate, UICollectionViewDataSource, RTTLLItemActionsDelegate>

@property (strong, nonatomic) IBOutlet UICollectionView     *fridgeCollectionView;
@property (strong, nonatomic) IBOutlet UIView               *emptyView;
@property (strong, nonatomic) IBOutlet UILabel              *emptyViewLabel;
@property (strong, nonatomic) IBOutlet UIImageView          *emptyViewArrowImage;
@property (strong, nonatomic) IBOutlet UIButton             *addButton;
@property (strong, nonatomic) IBOutlet UIImageView          *avatarImageView;
@property (strong, nonatomic) IBOutlet UILabel              *nameLabel;
@property (strong, nonatomic) IBOutlet UILabel              *fridgeTicketLabel;
@property (strong, nonatomic) IBOutlet UILabel              *uploadedCountLabel;
@property (strong, nonatomic) IBOutlet UILabel              *givenCountLabel;
@property (strong, nonatomic) IBOutlet UILabel              *wastedCountLabel;
@property (strong, nonatomic) IBOutlet UILabel              *uploadedLabel;
@property (strong, nonatomic) IBOutlet UILabel              *giveAwayLabel;
@property (strong, nonatomic) IBOutlet UILabel              *wastedLabel;
@property (strong, nonatomic) IBOutlet UIButton             *actionButton;
@property (strong, nonatomic) UIRefreshControl              *refreshControl;
@property (strong, nonatomic) NSArray                       *validItems;
@property (strong, nonatomic) NSArray                       *wastedItems;

@end

@implementation RTTLLFridgeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(loadUserData)
                                                 name:@"UserUpdatedNotification"
                                               object:nil];
    
    self.addButton.layer.cornerRadius       = self.addButton.frame.size.height / 2;
    self.addButton.layer.borderWidth        = 4;
    self.addButton.layer.borderColor        = COLOR_BRICK.CGColor;
    
    self.avatarImageView.layer.cornerRadius = self.avatarImageView.frame.size.height / 2;
    self.avatarImageView.layer.borderWidth  = 4;
    self.avatarImageView.layer.borderColor  = COLOR_TORTORA.CGColor;
    
    self.uploadedCountLabel.font            =
    self.givenCountLabel.font               =
    self.wastedCountLabel.font              = LATO_REG(20);
    
    self.uploadedLabel.font                 =
    self.giveAwayLabel.font                 =
    self.wastedLabel.font                   = LATO_LIGHT(9);
    
    self.nameLabel.font                     = LATO_LIGHT(30);
    self.fridgeTicketLabel.font             = LATO_LIGHT(10);
    self.emptyViewLabel.font                = LATO_LIGHT(10);
    
    self.fridgeCollectionView.contentInset  = UIEdgeInsetsMake(0, 0, 120, 0);
    
    self.refreshControl                     = [[UIRefreshControl alloc] init];
    self.refreshControl.tintColor           = COLOR_TORTORA;
    [self.refreshControl addTarget:self action:@selector(loadUserData)
             forControlEvents:UIControlEventValueChanged];
    [self.fridgeCollectionView addSubview:self.refreshControl];
    
    [self loadUserData];
    [self loadCategories];
}

- (void)loadUserData
{
    [self.refreshControl beginRefreshing];
    __block typeof(self) bself = self;
    if (self.user && ! [self.user isMe]) {
        //users/{id}/?{authentication_token}
        [[RTTLLHTTPSessionManager sharedManager] userWithId:self.user.id completion:^(RTTLLUser *user, NSError *error) {
            [bself drawDataOfUser:user error:error];
            [bself.refreshControl endRefreshing];
        }];
    } else if (!self.user || [self.user isMe]) {
        ///me?{authentication_token}
        [[RTTLLHTTPSessionManager sharedManager] meCompletion:^(RTTLLUser *user, NSError *error) {
            [bself drawDataOfUser:user error:error];
            [bself.refreshControl endRefreshing];
        }];
    }
}

- (void)loadCategories
{
    [[RTTLLHTTPSessionManager sharedManager] categoriesCompletion:^(NSArray *results, NSError *error) {
        [self.fridgeCollectionView reloadData];
    }];
}

- (void)toggleEmptyView
{
    BOOL empty                      = ![self.validItems count];
    self.emptyView.hidden           = !empty;
    self.emptyViewLabel.text        = [self.user isMe] ? EMPTY_ME_FRIGE_MSG : EMPTY_FRIGE_MSG(self.user.name);
    self.emptyViewArrowImage.hidden = ![self.user isMe];
    self.fridgeCollectionView.backgroundColor = empty ? [UIColor clearColor] : COLOR_BEIGE;
}

- (void)drawDataOfUser:(RTTLLUser*)user error:(NSError*)error
{
    if (error) {
         [SVProgressHUD showErrorWithStatus:[error errorDescription]];
        return;
    }
    self.user           = user;
    self.nameLabel.text = [NSString stringWithFormat:@"%@'S", [self.user.name uppercaseString]];
    self.validItems     = [self.user.fridge validItems];
    self.wastedItems    = [self.user.fridge expiredItems];

    self.uploadedCountLabel.text    = [@([self.user.fridge.items count]) stringValue];
    self.givenCountLabel.text       = [@([[self.user.fridge takenItems] count]) stringValue];
    self.wastedCountLabel.text      = [@([[self.user.fridge expiredItems] count]) stringValue];
    
    __block typeof(self) bself = self;
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:self.user.image_url]];
    [self.avatarImageView setImageWithURLRequest:request placeholderImage:self.avatarImageView.image success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
     {
         bself.avatarImageView.image    = image;
         bself.avatarImageView.alpha    = 0.0;
         [UIView animateWithDuration:1.0 animations:^{
             bself.avatarImageView.alpha = 1.0;
         }];
     } failure:nil];
    
    self.actionButton.hidden = NO;
    if ([self.user isMe]) {
        self.addButton.hidden = NO;
        [self.actionButton setImage:[UIImage imageNamed:@"edit_pf"] forState:UIControlStateNormal];
    } else {
        self.addButton.hidden = YES;
        [self.actionButton setImage:[UIImage imageNamed:@"button_close"] forState:UIControlStateNormal];
    }
    [self toggleEmptyView];
    [self.fridgeCollectionView reloadData];
}

- (void)pushItemControllerWithItem:(RTTLLItem*)item
{
    UINavigationController *navigationController    = [[self storyboard] instantiateViewControllerWithIdentifier:@"item"];
    RTTLLActionsViewController *itemViewController  = (RTTLLActionsViewController*)[navigationController visibleViewController];
    itemViewController.delegate     = self;
    itemViewController.isMine       = [self.user isMe];
    itemViewController.item         = item;
    itemViewController.user         = self.user;
    
    [self presentViewController:navigationController animated:YES completion:nil];
}

#pragma mark - UICollectionViewDataSource methods

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.validItems count];
}

-(UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    UICollectionReusableView *view = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"header" forIndexPath:indexPath];
    return view;
}

//TODO decoration view  http://markpospesel.wordpress.com/2012/12/11/decorationviews/

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    RTTLLItemViewCell *cell = (RTTLLItemViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
    
    RTTLLItem *item         = self.validItems[indexPath.row];;
    cell.nameLabel.text     = [item.name uppercaseString];
    RTTLLCategory *category = [RTTLLCategory categoryWithId:item.category_id];
    [cell setImageFromUrl:category.image_url];
    return cell;
}

#pragma mark - UICollectionViewDelegate methods

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(100, 158);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    RTTLLItem *item = self.validItems[indexPath.row];

    [self pushItemControllerWithItem:item];
}

#pragma mark - Navigation

-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    if ([identifier isEqualToString:@"settings"] && ![self.user isMe]) {
        [self dismissViewControllerAnimated:YES completion:nil];
        return NO;
    }
    return YES;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"item"]) {
        [self pushItemControllerWithItem:nil];
    }
    
    if ([[segue identifier] isEqualToString:@"settings"] && [self.user isMe]) {
        RTTLLSettingsViewController *settingsViewController = (RTTLLSettingsViewController*)[[segue destinationViewController] visibleViewController];
        settingsViewController.user = self.user;
    }
}


#pragma mark - Item View Controller Delegate

-(void)didDismissItemViewController:(RTTLLItemViewController *)itemController
{
    [self loadUserData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
