//
//  RTTLLFridgesViewController.m
//  Ratatouille
//
//  Created by Giorgia Marenda on 12/8/13.
//  Copyright (c) 2013 Giorgia Marenda. All rights reserved.
//

#import "RTTLLFridgesViewController.h"

#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import "RTTLLFridgeViewController.h"
#import "RTTLLActionsViewController.h"
#import "RTTLLViewController.h"
#import "RTTLLNavigationController.h"
#import "UIImageView+AFNetworking.h"
#import "RTTLLHTTPSessionManager.h"
#import "RTTLLShareManager.h"
#import "KPTreeController.h"
#import "UIView+Additions.h"
#import "RTTLLFridgeCell.h"
#import "RTTLLSearchCell.h"
#import "KPAnnotation.h"
#import "RTTLLFridge.h"
#import "RTTLLUser.h"
#import "RTTLLPin.h"

static UIEdgeInsets pinPadding = { 60.f, 30.f, 30.f, 30.f };

static NSString *cellIdentifer          = @"CellIdentifier";
static NSString *searchCellIdentifer    = @"searchCellIdentifier";

static CGRect mapOriginalFrame;
static CGRect mapFullFrame;
static CGFloat offset = -50.0f;
BOOL isMaximizing;


@interface RTTLLFridgesViewController () <MKMapViewDelegate, UITableViewDataSource, UITableViewDelegate, UISearchDisplayDelegate, UISearchBarDelegate, KPTreeControllerDelegate, CLLocationManagerDelegate>

@property (nonatomic, strong) NSArray                   *users;
@property (nonatomic, strong) NSMutableArray            *filteredItems;
@property (nonatomic, strong) IBOutlet MKMapView        *mapView;
@property (nonatomic, strong) IBOutlet UITableView      *tableView;
@property (nonatomic, strong) IBOutlet UIButton         *closeButton;
@property (nonatomic, strong) IBOutlet UIButton         *currentLocationButton;
@property (nonatomic, strong) UITapGestureRecognizer    *tapGesture;
@property (nonatomic, assign) MKCoordinateRegion        currentRegion;
@property (strong, nonatomic) IBOutlet UIButton         *searchAreaButton;
@property (strong, nonatomic) IBOutlet UIView           *emptyView;
@property (strong, nonatomic) IBOutlet UILabel          *emptyLabel;
@property (strong, nonatomic) CALayer                   *mapBottomBorder;
@property (strong, nonatomic) UIRefreshControl          *refreshControl;
@property (strong, nonatomic) CLLocationManager         *locationManager;
@property (nonatomic, strong) KPTreeController          *treeController;

@end


@implementation RTTLLFridgesViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setupSearchController];
    
    [self setupMapView];
    
    self.searchAreaButton.layer.borderColor         = COLOR_LIGHT_LOBSTER.CGColor;
    self.searchAreaButton.layer.borderWidth         = 3;
    self.searchAreaButton.titleLabel.font           = LATO_REG(12);
    self.closeButton.layer.borderColor              = COLOR_BEIGE.CGColor;
    self.closeButton.layer.borderWidth              = 3;
    self.currentLocationButton.layer.borderColor    = COLOR_BEIGE.CGColor;
    self.currentLocationButton.layer.borderWidth    = 3;
    self.emptyLabel.font                            = LATO_REG(10);
    
    self.treeController                     = [[KPTreeController alloc] initWithMapView:self.mapView];
    self.treeController.delegate            = self;
    self.treeController.animationOptions    = UIViewAnimationOptionCurveEaseOut;
    
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    [self.locationManager requestWhenInUseAuthorization];
    self.mapView.showsUserLocation = YES;
}


#pragma mark -
#pragma mark Initial setups

- (void)setupMapView
{
    CGSize screen = [[UIScreen mainScreen] bounds].size;
    UIView *tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(64.0, 0.0, self.view.frame.size.width, screen.height / 2 - 90)];
    self.tableView.tableHeaderView = tableHeaderView;
    
    mapOriginalFrame = CGRectMake(0.0, offset, screen.width, screen.height / 2 + 18);
    
    CGSize size     = [UIScreen mainScreen].bounds.size;
    mapFullFrame    = CGRectMake(0, 0, size.width, size.height);
    
    isMaximizing    = NO;
    
    self.mapView                = [[MKMapView alloc] initWithFrame:mapOriginalFrame];
    self.mapView.delegate       = self;
    self.mapView.rotateEnabled  = NO;
    self.mapBottomBorder        = [self.mapView bottomBorderOfWidth:1 andColor:COLOR_BEIGE];
    
    [self.view insertSubview:self.mapView aboveSubview:self.tableView];
    
    self.tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(mapTap:)];
    [self.tapGesture setCancelsTouchesInView:NO];
    self.tapGesture.numberOfTapsRequired    = 1;
    self.tapGesture.numberOfTouchesRequired = 1;
    
    [self.mapView addGestureRecognizer:self.tapGesture];
    [self.mapView setShowsUserLocation:YES];
}

- (void)setupSearchController
{
    if(![[NSUserDefaults standardUserDefaults] objectForKey:@"radius"]) {
        [[NSUserDefaults standardUserDefaults] setObject:@"15000" forKey:@"radius"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    self.searchDisplayController.displaysSearchBarInNavigationBar = YES;
    [self.searchDisplayController.searchResultsTableView registerClass:[RTTLLSearchCell class] forCellReuseIdentifier:searchCellIdentifer];
    self.searchDisplayController.searchResultsTableView.rowHeight       = 70;
    self.searchDisplayController.searchResultsTableView.separatorStyle  = UITableViewCellSeparatorStyleNone;
    self.searchDisplayController.searchResultsTableView.backgroundColor = COLOR_LIGHT_BEIGE;
    
    self.searchDisplayController.searchBar.delegate         = self;
    self.searchDisplayController.searchResultsDataSource    = self;
    self.searchDisplayController.searchResultsDelegate      = self;
}

#pragma mark -
#pragma mark Load datas

- (void)updateRegion
{
    [self reloadDataFrom:self.currentRegion.center];
}

- (void)reloadDataFrom:(CLLocationCoordinate2D)location
{
    self.searchAreaButton.hidden = YES;
    
    __block typeof(self) bself = self;
    
    [[RTTLLHTTPSessionManager sharedManager] usersNear:location userLocation:self.mapView.userLocation.coordinate radius:[[NSUserDefaults standardUserDefaults] objectForKey:@"radius"] completion:^(NSArray *results, NSError *error) {
        if(error) {
            [SVProgressHUD showErrorWithStatus:[error errorDescription]];
            return;
        }
        bself.users = results;
        if ([results count]) {
            self.tableView.hidden = NO;
            self.emptyView.hidden = YES;
            [bself.tableView reloadData];
        } else {
            self.tableView.hidden = YES;
            self.emptyView.hidden = NO;
        }
        [bself loadPins];
        
    }];
}

#pragma mark -
#pragma mark Pins

- (void)loadPins
{
    if ([self.users count]) {
        [self.mapView removeAnnotations:[self.mapView annotations]];
        [self.treeController setAnnotations:[self annotations]];
    } else {
        [self.mapView setRegion:self.currentRegion animated:YES];
    }
    [self minimizeMapView];
    
}

- (NSArray *)annotations {
    
    NSMutableArray *annotations = [NSMutableArray array];
    MKMapRect zoomRect = MKMapRectNull;
    
    for (RTTLLUser *user in self.users) {
        if ([user.fridge.lat length] && [user.fridge.lng length]) {
            RTTLLPin *ann   = [[RTTLLPin alloc] init];
            ann.title       = [NSString stringWithFormat:@"Fridge of %@", user.name];
            ann.userId      = user.id;
            ann.coordinate  = CLLocationCoordinate2DMake([user.fridge.lat doubleValue], [user.fridge.lng doubleValue]);
            MKMapPoint annotationPoint  = MKMapPointForCoordinate(ann.coordinate);
            MKMapRect pointRect         = MKMapRectMake(annotationPoint.x, annotationPoint.y, 0.1, 0.1);
            zoomRect                    = MKMapRectUnion(zoomRect, pointRect);
            
            [annotations addObject:ann];
        }
    }
    [self.mapView setVisibleMapRect:zoomRect edgePadding:pinPadding animated:YES];
    return annotations;
}


#pragma mark -
#pragma mark Actions

- (IBAction)searchInThisAreaTapped:(id)sender
{
    self.searchAreaButton.hidden = NO;
    [self updateRegion];
}

- (IBAction)minimizeMap:(id)sender
{
    [self minimizeMapView];
}

- (IBAction)centerCurrentLocation:(id)sender
{
    self.currentRegion = MKCoordinateRegionMakeWithDistance(self.mapView.userLocation.coordinate, 10.0, 10.0);
    [self.mapView setRegion:self.currentRegion animated:YES];

    [self reloadDataFrom:self.currentRegion.center];
}

- (void)pushFridgeOfUser:(RTTLLUser*)user
{
    RTTLLFridgeViewController *incoming = [[self storyboard] instantiateViewControllerWithIdentifier:@"fridge"];
    incoming.user                       = user;
    [self.navigationController presentViewController:incoming animated:YES completion:nil];
}

- (IBAction)showStats:(id)sender
{
    [NSNotificationCenter.defaultCenter postNotificationName: ShowStatsNotification object: nil];
}

- (IBAction)showProfile:(id)sender
{
    [NSNotificationCenter.defaultCenter postNotificationName: ShowProfileNotification object: nil];
}

#pragma mark -
#pragma mark Map View Delegate

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view {
    
    if([view.annotation isKindOfClass:[KPAnnotation class]]){
        
        KPAnnotation *cluster = (KPAnnotation *)view.annotation;
        
        if(cluster.annotations.count > 1){
            [self.mapView setRegion:MKCoordinateRegionMakeWithDistance(cluster.coordinate,
                                                                       cluster.radius * 2.5f,
                                                                       cluster.radius * 2.5f)
                           animated:YES];
        }
    }
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    MKAnnotationView *pinView = nil;
    
    if([annotation isKindOfClass:[KPAnnotation class]]){
        
        KPAnnotation *a = (KPAnnotation *)annotation;
        
        if([annotation isKindOfClass:[MKUserLocation class]]){
            return nil;
        }
        
        if([a isCluster]){
            
            pinView = (MKAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:@"cluster"];
            
            if(!pinView){
                pinView = [[MKAnnotationView alloc] initWithAnnotation:a reuseIdentifier:@"cluster"];
            }
            
            pinView.image = [UIImage imageNamed:@"pin_multi"];
        }
        else {
            
            pinView = (MKAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:@"annotationView"];
            
            if(!pinView){
                pinView = [[MKAnnotationView alloc] initWithAnnotation:[a.annotations anyObject]
                                                    reuseIdentifier:@"annotationView"];
            }
            
            UIButton* rightButton               = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
            rightButton.tintColor               = COLOR_LOBSTER;
            pinView.rightCalloutAccessoryView   = rightButton;
            pinView.image                       = [UIImage imageNamed:@"pin"];
        }
        
        pinView.canShowCallout = YES;
        
    }
    else if([annotation isKindOfClass:[RTTLLPin class]]) {
        
        if (![annotation isKindOfClass:[MKUserLocation class]]) {
        
            pinView                             = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"annotationView"];
            pinView.annotation                  = annotation;
            pinView.canShowCallout              = YES;
            UIButton* rightButton               = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
            rightButton.tintColor               = COLOR_LOBSTER;
            pinView.rightCalloutAccessoryView   = rightButton;
            pinView.image                       = [UIImage imageNamed:@"pin"];
        }
    }
    
    return pinView;
}


- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
    KPAnnotation *ann = (KPAnnotation*)view.annotation;
    RTTLLPin *pin = (RTTLLPin*)[[ann.annotations allObjects] lastObject];

    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id == %d", pin.userId];
    NSArray *usersPin = [self.users filteredArrayUsingPredicate:predicate];
    if ([usersPin count]) {
        [self pushFridgeOfUser:[usersPin lastObject]];
        return;
    }
    [SVProgressHUD showErrorWithStatus:@"Fridge details not found."];
}

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
    if (!self.currentRegion.center.latitude && !self.currentRegion.center.longitude) {
        self.currentRegion =  MKCoordinateRegionMakeWithDistance(self.mapView.userLocation.location.coordinate, 1000.0, 1000.0);
        [self reloadDataFrom:userLocation.coordinate];
    }
}

-(void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated
{
    if (isMaximizing) {
        self.searchAreaButton.hidden = NO;
    }
    [self.treeController refresh:animated];
}

#pragma mark -
#pragma mark Table View Data Source

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        return [self.filteredItems count];
    }
    return [self.users count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        
        RTTLLSearchCell *cell   = [self.tableView dequeueReusableCellWithIdentifier:searchCellIdentifer];
        
        RTTLLItem *item         = self.filteredItems[indexPath.row];
        cell.nameLabel.text     = [item.name uppercaseString];
        
        RTTLLCategory *category = [RTTLLCategory categoryWithId:item.category_id];
        NSURL *imageURL = [NSURL URLWithString:category.image_url];
        if (imageURL!= nil) {
            [cell.itemImageView setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@"empty_category_fridge"]];
        }

        [cell.distanceLabel setText:item.distance];
        [cell.descLabel     setText:item.desc];
        
        return cell;
        
        
    } else {
        RTTLLFridgeCell *cell = [self.tableView dequeueReusableCellWithIdentifier:cellIdentifer forIndexPath:indexPath];
        
        RTTLLUser *user         = self.users[indexPath.row];
        cell.nameLabel.text     = [NSString stringWithFormat:@"%@'S FRIDGE", [user.name uppercaseString]];
        [cell setImageFromUrl:user.image_url];
        [cell.distanceLabel setText:user.fridge.distance];
        [cell.itemsLabel    setText:[NSString stringWithFormat:@"%@ items", user.fridge.items_count]];
        return cell;
    }
    return nil;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        
        RTTLLNavigationController *nav          = [[self storyboard] instantiateViewControllerWithIdentifier:@"item"];
        RTTLLActionsViewController *container   = (RTTLLActionsViewController*)nav.visibleViewController;
        RTTLLItem *item                         = self.filteredItems[indexPath.row];
        container.item                          = item;
        
        [self presentViewController:nav animated:YES completion:nil];
    } else {
        [self pushFridgeOfUser:self.users[indexPath.row]];
    }
}

#pragma mark - Content Filtering

-(void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    // Update the filtered array based on the search text and scope.
    // Remove all objects from the filtered search array
    [self.filteredItems removeAllObjects];
    
    // Filter the array
    [SVProgressHUD showWithStatus:@"Searching..."];
    
    __block typeof(self) bself = self;
    NSString *radius = [[NSUserDefaults standardUserDefaults] objectForKey:@"radius"];
    
    [[RTTLLHTTPSessionManager sharedManager] itemsNear:self.mapView.userLocation.coordinate radius:radius query:searchText completion:^(NSArray *results, NSError *error)
     {
         [SVProgressHUD dismiss];
         if (error) {
             [SVProgressHUD showErrorWithStatus:[error errorDescription]];
             return;
         }
         bself.filteredItems = [NSMutableArray arrayWithArray:results];
         [[bself.searchDisplayController searchResultsTableView] reloadData];
     }];
    
}

#pragma mark UISearchDisplayController Delegate Methods

-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    // Tells the table data source to reload when text changes
    // Return YES to cause the search result table view to be reloaded.
    return NO;
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self filterContentForSearchText:searchBar.text scope:nil];
    
}

#pragma mark - KPTreeControllerDelegate

- (void)treeController:(KPTreeController *)tree configureAnnotationForDisplay:(KPAnnotation *)annotation
{
    if (annotation.annotations.count > 1) {
        annotation.title = [NSString stringWithFormat:@"%@ fridges", @(annotation.annotations.count)];
    } else {
        RTTLLPin *pin           = (RTTLLPin*)[[annotation.annotations allObjects] lastObject];
        annotation.title        = pin.title;
        annotation.subtitle     = pin.subtitle;
    }
}

#pragma mark - ScrollView Delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    if (scrollView == self.tableView) {
        CGFloat scrollOffset = scrollView.contentOffset.y;
        
        if (!CGRectEqualToRect(self.mapView.frame, mapFullFrame)) {
            
            CGRect mapFrame = self.mapView.frame;
            if (scrollOffset < 0) {
            
                if (!isMaximizing && scrollOffset < - 100) {
                    [self maximizeMapView];
                    return;
                }
                mapFrame.origin.y = offset - ((scrollOffset/2));
                
            } else {
                mapFrame.origin.y = offset - scrollOffset;
            }
            self.mapView.frame = mapFrame;
        }
    }
}

- (void)mapTap:(UITapGestureRecognizer* )recognizer
{
    if (CGRectEqualToRect(self.mapView.frame, mapFullFrame)) {
        [self minimizeMapView];
    } else {
        [self maximizeMapView];
    }
}

- (void) maximizeMapView
{
    isMaximizing = YES;
    [self.mapView removeGestureRecognizer:self.tapGesture];
    [UIView animateWithDuration:0.3 animations:^{
        self.mapBottomBorder.hidden         = YES;
        self.closeButton.hidden             = NO;
        self.currentLocationButton.hidden   = NO;
        self.mapView.frame = mapFullFrame;
    } completion:nil];
}

- (void) minimizeMapView
{
    isMaximizing = NO;
    [self.mapView addGestureRecognizer:self.tapGesture];
    self.mapBottomBorder.hidden = NO;
    [UIView animateWithDuration:0.3 animations:^{
        self.closeButton.hidden             = YES;
        self.searchAreaButton.hidden        = YES;
        self.currentLocationButton.hidden   = YES;
        self.mapView.frame = mapOriginalFrame;
    } completion:nil];
}

- (IBAction)onShare:(id)sender
{
    NSString *text  = SHARE_TEXT;
    UIImage *image  = [UIImage imageNamed:@"icon"];
    NSURL *url      = [NSURL URLWithString:APP_STORE_LINK];
    [RTTLLShareManager shareActionFrom:self withText:text image:image url:url];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
