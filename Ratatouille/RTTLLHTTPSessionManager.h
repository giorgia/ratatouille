//
//  RTTLLHTTPSessionManager.h
//  Ratatouille
//
//  Created by Giorgia Marenda on 11/3/13.
//  Copyright (c) 2013 Giorgia Marenda. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>
#import "AFHTTPSessionManager.h"
#import "RTTLLUser.h"
#import "RTTLLItem.h"
#import "RTTLLStats.h"

@interface RTTLLHTTPSessionManager : AFHTTPSessionManager

@property (nonatomic, strong) NSCache *cacheData;

+ (RTTLLHTTPSessionManager *)sharedManager;

+ (void)setUserToken:(NSString*)userToken andUserEmail:(NSString*)email;
+ (NSString*)userEmail;
+ (NSString*)userToken;
+ (void)setUserPositionConfirmed:(BOOL)userPositionConfirmed;
+ (BOOL)userPositionConfirmed;
+ (void)deleteUserInfo;

//AUTHENTICATION
- (NSURLSessionDataTask *)signUpWithInfo:(NSDictionary*)info
                               andAvatar:(UIImage*)avatar
                              completion:( void (^)(NSError *error) )completion;

- (NSURLSessionDataTask *)signInWithInfo:(NSDictionary*)info
                              completion:( void (^)(NSError *error) )completion;

- (NSURLSessionDataTask *)signOutCompletion:(void (^)(NSError *error) )completion;


//USERS
- (NSURLSessionDataTask *)meCompletion:( void (^)(RTTLLUser *user, NSError *error) )completion;

- (NSURLSessionDataTask *)deleteMeCompletion:(void (^)(NSError *error) )completion;


- (NSURLSessionDataTask *)usersNear:(CLLocationCoordinate2D)location
                       userLocation:(CLLocationCoordinate2D)userLocation
                             radius:(NSString*)radius
                         completion:( void (^)(NSArray *results, NSError *error) )completion;

- (NSURLSessionDataTask *)updateMeWithInfo:(NSDictionary*)info
                                 andAvatar:(UIImage*)avatar
                                completion:( void (^)(NSError *error) )completion;

- (NSURLSessionDataTask *)userWithId:(NSInteger)userId completion:( void (^)(RTTLLUser *user, NSError *error) )completion;

//FRIGES
- (NSURLSessionDataTask *)updateFridgeLocation:(CLLocationCoordinate2D)location completion:( void (^)(NSError *error) )completion;


//ITEMS
- (NSURLSessionDataTask *)itemsNear:(CLLocationCoordinate2D)location radius:(NSString*)radius query:(NSString*)query completion:( void (^)(NSArray *results, NSError *error) )completion;

- (NSURLSessionDataTask *)itemWithId:(NSInteger)itemId completion:( void (^)(RTTLLItem *item, NSError *error) )completion;

- (NSURLSessionDataTask *)createItemWithImage:(UIImage*)image
                                   completion:(void (^)(RTTLLItem* item, NSError *error))completion;

- (NSURLSessionDataTask *)createItemWithInfo:(NSDictionary*)info
                                  completion:( void (^)(NSError *error) )completion;

- (NSURLSessionDataTask *)updateItemWithId:(NSInteger)itemId
                                  withInfo:(NSDictionary*)info
                                  andImage:(UIImage*)image
                                completion:( void (^)(NSError *error))completion;

- (NSURLSessionDataTask *)deleteItemWithId:(NSInteger)itemId completion:( void (^)(NSError *error))completion;

//CATEGORIES
- (NSURLSessionDataTask *)categoriesCompletion:( void (^)(NSArray *results, NSError *error) )completion;

//STATS
- (NSURLSessionDataTask *)statsCompletion:( void (^)(RTTLLStats *stats, NSError *error) )completion;


- (void)contentFromURL:(NSURL*)url
      complentionBlock:(void (^)(NSString *content, NSError *error))block;

@end
