//
//  RTTLLHTTPSessionManager.m
//  Ratatouille
//
//  Created by Giorgia Marenda on 11/3/13.
//  Copyright (c) 2013 Giorgia Marenda. All rights reserved.
//

#import "RTTLLAFJSONResponseSerializer.h"
#import "RTTLLHTTPSessionManager.h"
#import "RTTLLAppDelegate.h"
#import "JSONModelArray.h"
#import "RTTLLFridge.h"

@interface RTTLLHTTPSessionManager ()

@end

@implementation RTTLLHTTPSessionManager

+ (RTTLLHTTPSessionManager *)sharedManager {
    static RTTLLHTTPSessionManager *_sharedManager = nil;

    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLCache *cache = [[NSURLCache alloc] initWithMemoryCapacity:10 * 1024 * 1024
                                                          diskCapacity:50 * 1024 * 1024
                                                              diskPath:nil];
        [config setURLCache:cache];
        
        _sharedManager = [[RTTLLHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:BASE_URL] sessionConfiguration:config];

        _sharedManager.requestSerializer = [AFJSONRequestSerializer serializer];
        [_sharedManager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [_sharedManager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        
        _sharedManager.responseSerializer = [RTTLLAFJSONResponseSerializer serializer];
        
    });
    
    return _sharedManager;
}

-(instancetype)initWithBaseURL:(NSURL *)url sessionConfiguration:(NSURLSessionConfiguration *)configuration
{
    if (self = [super initWithBaseURL:url sessionConfiguration:configuration]) {
        self.cacheData = [[NSCache alloc] init];
    }
    return self;
}

+ (void)setUserToken:(NSString*)userToken andUserEmail:(NSString*)email
{
    [[NSUserDefaults standardUserDefaults] setObject:userToken forKey:AUTH_TOKEN_KEY];
    [[NSUserDefaults standardUserDefaults] setObject:email forKey:AUTH_USER_EMAIL];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString*)userToken
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:AUTH_TOKEN_KEY];
}

+ (void)setUserPositionConfirmed:(BOOL)userPositionConfirmed
{
    [[NSUserDefaults standardUserDefaults] setBool:userPositionConfirmed forKey:POSITION_CONFIRM_KEY];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (BOOL)userPositionConfirmed
{
    return [[NSUserDefaults standardUserDefaults] boolForKey:POSITION_CONFIRM_KEY];
}

+ (NSString*)userEmail
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:AUTH_USER_EMAIL];
}

+ (void)deleteUserInfo
{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:AUTH_TOKEN_KEY];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:AUTH_USER_EMAIL];
    [[NSUserDefaults standardUserDefaults] synchronize];
}



#pragma mark AUTENTICATION

// User sign-up [/signup]
// Signs up a new user [POST]

- (NSURLSessionDataTask *)signUpWithInfo:(NSDictionary*)info andAvatar:(UIImage*)avatar completion:(void (^)(NSError *error) )completion
{
    NSURLSessionDataTask *task = [self POST:SIGNUP parameters:info constructingBodyWithBlock:^(id<AFMultipartFormData> formData)
    {
        if (avatar) {
            [formData appendPartWithFileData:UIImagePNGRepresentation(avatar)
                                        name:@"image_url"
                                    fileName:@"avatar.png"
                                    mimeType:@"image/png"];
        }
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
        
            [RTTLLHTTPSessionManager setUserToken:responseObject[USER_KEY][AUTH_TOKEN_KEY] andUserEmail:responseObject[USER_KEY][USER_EMAIL]];
            completion(nil);
            
        });
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            completion(error);
        });
    }];
    
    return task;
}

// User sign-in [/signin]
// Signs in an existing user [POST]

- (NSURLSessionDataTask *)signInWithInfo:(NSDictionary*)info completion:(void (^)(NSError *error) )completion
{
    NSURLSessionDataTask *task = [self POST:SIGNIN parameters:@{USER_KEY : info} success:^(NSURLSessionDataTask *task, id responseObject) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if (responseObject[USER_KEY][AUTH_TOKEN_KEY] && responseObject[USER_KEY][USER_EMAIL]) {
                [RTTLLHTTPSessionManager setUserToken:responseObject[USER_KEY][AUTH_TOKEN_KEY] andUserEmail:responseObject[USER_KEY][USER_EMAIL]];
                    completion(nil);
            }
        });
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            completion(error);
        });
    }];
    
    return task;
}

// User delete [/me]
// Delete an existing user [DELETE]

- (NSURLSessionDataTask *)deleteMeCompletion:(void (^)(NSError *error) )completion
{
    NSDictionary *parameters = @{AUTH_TOKEN_KEY  : [RTTLLHTTPSessionManager userToken],
                                 AUTH_USER_EMAIL : [RTTLLHTTPSessionManager userEmail]};
    
    NSURLSessionDataTask *task = [self DELETE:ME parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [RTTLLHTTPSessionManager deleteUserInfo];
            completion(nil);
        });
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            completion(error);
        });
    }];
    
    return task;
}


// User logout [/signout]
// Sign out existing user [DELETE]

- (NSURLSessionDataTask *)signOutCompletion:(void (^)(NSError *error) )completion
{
    NSDictionary *parameters = @{AUTH_TOKEN_KEY  : [RTTLLHTTPSessionManager userToken],
                                 AUTH_USER_EMAIL : [RTTLLHTTPSessionManager userEmail]};
    
    NSURLSessionDataTask *task = [self DELETE:SIGNOUT parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
           
                [RTTLLHTTPSessionManager deleteUserInfo];
                completion(nil);
            
        });
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            completion(error);
        });
    }];
    
    return task;
}

#pragma mark USERS

// User me [/me?{authentication_token, authentication_email}]
// Get current user informations [GET]

- (NSURLSessionDataTask *)meCompletion:( void (^)(RTTLLUser *user, NSError *error) )completion
{
    if ([RTTLLHTTPSessionManager userToken] && [RTTLLHTTPSessionManager userEmail]) {
        
        NSDictionary *parameters = @{AUTH_TOKEN_KEY  : [RTTLLHTTPSessionManager userToken],
                                     AUTH_USER_EMAIL : [RTTLLHTTPSessionManager userEmail]};
        
        NSURLSessionDataTask *task = [self GET:ME parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject)
        {
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)task.response;
          
            if (httpResponse.statusCode == 200) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    NSError *error = nil;
                    RTTLLUser *user = [[RTTLLUser alloc] initWithDictionary:responseObject[USER_KEY] error:&error];
                    if (!error) {
                        [RTTLLUser setMyUserId:user.id];
                    }
                    completion(user, error);
                    
                });
            } else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    completion(nil, nil);
                });
            }
            
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    completion(nil, error);
                });
        }];
        return task;
        
    }
    return nil;
}

// User [/me?{authentication_token, authentication_email}]
// Update current user informations [PUT]

- (NSURLSessionDataTask *)updateMeWithInfo:(NSDictionary*)info andAvatar:(UIImage*)avatar completion:( void (^)(NSError *error) )completion
{
    NSDictionary *parameters = @{AUTH_TOKEN_KEY     : [RTTLLHTTPSessionManager userToken],
                                 AUTH_USER_EMAIL    : [RTTLLHTTPSessionManager userEmail],
                                @"_method"          : @"put"};
    
    NSMutableDictionary *combinedAttributes = [NSMutableDictionary dictionaryWithDictionary:parameters];
    if (info) {
        [combinedAttributes addEntriesFromDictionary:info];
    }
    NSURLSessionDataTask *task = [self POST:ME parameters:combinedAttributes constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        if (avatar) {
            [formData appendPartWithFileData:UIImageJPEGRepresentation(avatar, 0.5)
                                        name:@"image_url"
                                    fileName:@"avatar.jpg"
                                    mimeType:@"image/jpeg"];
        }
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            completion(nil);
        });
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            completion(error);
        });
    }];
    
    return task;
}

//  Users Collection [/users?{lat, lng, radius, authentication_token, authentication_email}]
//  List all users [GET]

- (NSURLSessionDataTask *)usersNear:(CLLocationCoordinate2D)location userLocation:(CLLocationCoordinate2D)userLocation radius:(NSString*)radius completion:( void (^)(NSArray *results, NSError *error) )completion
{
    if ([RTTLLHTTPSessionManager userToken]) {
        NSDictionary *parameters = @{@"lat"         : [@(location.latitude) stringValue],
                                     @"lng"         : [@(location.longitude) stringValue],
                                     @"user_lat"    : [@(userLocation.latitude) stringValue],
                                     @"user_lng"    : [@(userLocation.longitude) stringValue],
                                     @"radius"      : radius,
                                     AUTH_TOKEN_KEY : [RTTLLHTTPSessionManager userToken],
                                     AUTH_USER_EMAIL: [RTTLLHTTPSessionManager userEmail]};
        
        NSURLSessionDataTask *task = [self GET:USERS parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                    
                    JSONModelArray  *users = [[JSONModelArray alloc] initWithArray:responseObject modelClass:[RTTLLUser class]];
                    completion([users toArray], nil);
                });
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                completion(nil, error);
            });
        }];
        return task;
        
    }
    return nil;
}

// User [/users/{id}/?{authentication_token, authentication_email}]
// A single user with fridge object with all its items [GET]

- (NSURLSessionDataTask *)userWithId:(NSInteger)userId completion:( void (^)(RTTLLUser *user, NSError *error) )completion
{
    if ([RTTLLHTTPSessionManager userToken]) {
        
        NSDictionary *parameters = @{AUTH_TOKEN_KEY  : [RTTLLHTTPSessionManager userToken],
                                     AUTH_USER_EMAIL : [RTTLLHTTPSessionManager userEmail]};
        
        NSURLSessionDataTask *task = [self GET:USERS_WITH_ID(@(userId)) parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
            
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)task.response;
            if (httpResponse.statusCode == 200) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    NSError *error = nil;
                    RTTLLUser *user = [[RTTLLUser alloc] initWithDictionary:responseObject[USER_KEY] error:&error];
                    completion(user, error);
                    
                });
            } else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    completion(nil, nil);
                });
            }
            
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                completion(nil, error);
            });
        }];
        return task;
        
    }
    return nil;
}

#pragma mark FRIDGE

// User [/fridge/?{authentication_token, authentication_email}]
// Update user fridge location [PUT]

- (NSURLSessionDataTask *)updateFridgeLocation:(CLLocationCoordinate2D)location completion:( void (^)(NSError *error) )completion
{
    NSDictionary *parameters = @{@"lat"             : [@(location.latitude) stringValue],
                                 @"lng"             : [@(location.longitude) stringValue],
                                 AUTH_TOKEN_KEY     : [RTTLLHTTPSessionManager userToken],
                                 AUTH_USER_EMAIL    : [RTTLLHTTPSessionManager userEmail]};
    
    NSURLSessionDataTask *task = [self PUT:FRIDGE parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        [RTTLLHTTPSessionManager setUserPositionConfirmed:YES];
        dispatch_async(dispatch_get_main_queue(), ^{
            completion( nil);
        });
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            completion( error);
        });
    }];
    return task;
}

#pragma mark ITEMS

// Items [/items?{lat,lng, radius, query, authentication_token, authentication_email}]
// List all searchable items [GET]

- (NSURLSessionDataTask *)itemsNear:(CLLocationCoordinate2D)location radius:(NSString*)radius query:(NSString*)query completion:( void (^)(NSArray *results, NSError *error) )completion
{
    if ([RTTLLHTTPSessionManager userToken]) {
        NSDictionary *parameters = @{@"user_lat"         : [@(location.latitude) stringValue],
                                     @"user_lng"         : [@(location.longitude) stringValue],
                                     @"radius"           : radius,
                                     @"query"            : query,
                                     AUTH_TOKEN_KEY      : [RTTLLHTTPSessionManager userToken],
                                     AUTH_USER_EMAIL     : [RTTLLHTTPSessionManager userEmail]};
        
        NSURLSessionDataTask *task = [self GET:ITEMS parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                JSONModelArray *items = [[JSONModelArray alloc] initWithArray:responseObject[ITEMS_KEY] modelClass:[RTTLLItem class]];
                
                completion([items toArray], nil);
            });
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                completion(nil, error);
            });
        }];
        return task;
        
    }
    return nil;

}

// Items [/items/{id}/?{authentication_token, authentication_email}]
// Get an item details [GET]

- (NSURLSessionDataTask *)itemWithId:(NSInteger)itemId completion:( void (^)(RTTLLItem *item, NSError *error))completion
{
    NSDictionary *parameters = @{AUTH_TOKEN_KEY  : [RTTLLHTTPSessionManager userToken],
                                 AUTH_USER_EMAIL : [RTTLLHTTPSessionManager userEmail]};
    
    NSURLSessionDataTask *task = [self GET:ITEMS_WITH_ID(@(itemId)) parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)task.response;
        if (httpResponse.statusCode == 200) {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                NSError *error = nil;
                RTTLLItem *item = [[RTTLLItem alloc] initWithDictionary:responseObject[ITEM_KEY] error:&error];
                completion(item, error);
            });
            
        } else {
            dispatch_async(dispatch_get_main_queue(), ^{
                completion(nil, nil);
            });
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            completion(nil, error);
        });
    }];
    return task;

}

// Items [/items/?{lat,lng, radius, query, authentication_token, authentication_email}
// New item creation [POST]

- (NSURLSessionDataTask *)createItemWithInfo:(NSDictionary*)info completion:( void (^)(NSError *error))completion
{
    NSDictionary *parameters = @{AUTH_TOKEN_KEY     : [RTTLLHTTPSessionManager userToken],
                                 AUTH_USER_EMAIL    : [RTTLLHTTPSessionManager userEmail]};
    
    NSMutableDictionary *combinedAttributes = [NSMutableDictionary dictionaryWithDictionary:parameters];
    [combinedAttributes addEntriesFromDictionary:@{ITEM_KEY: info}];
   
    
    NSURLSessionDataTask *task = [self POST:ITEMS parameters:combinedAttributes success:^(NSURLSessionDataTask *task, id responseObject) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            completion(nil);
        });
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            completion(error);
        });
    }];
    
    return task;
}

- (NSURLSessionDataTask *)createItemWithImage:(UIImage*)image completion:(void (^)(RTTLLItem* item, NSError *error))completion
{
    NSDictionary *parameters = @{AUTH_TOKEN_KEY     : [RTTLLHTTPSessionManager userToken],
                                 AUTH_USER_EMAIL    : [RTTLLHTTPSessionManager userEmail]};
    
    
     NSURLSessionDataTask *task = [self POST:ITEMS parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData)
    {
        [formData appendPartWithFileData:UIImageJPEGRepresentation(image, 0.6)
                                    name:@"image_url"
                                fileName:@"imageItem.jpg"
                                mimeType:@"image/jpeg"];
        
    } success:^(NSURLSessionDataTask *task, id responseObject)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            NSError *error = nil;
            RTTLLItem *item = [[RTTLLItem alloc] initWithDictionary:responseObject[ITEM_KEY] error:&error];
            completion(item, error);
            
        });
    } failure:^(NSURLSessionDataTask *task, NSError *error)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            completion(nil, error);
        });
    }];
    
    return task;
}


// Items [/items/{id}/?{authentication_token, authentication_email}]
// Edit an item [PUT]

- (NSURLSessionDataTask *)updateItemWithId:(NSInteger)itemId withInfo:(NSDictionary*)info andImage:(UIImage*)image completion:( void (^)(NSError *error))completion
{
    NSDictionary *parameters = @{AUTH_TOKEN_KEY     : [RTTLLHTTPSessionManager userToken],
                                 AUTH_USER_EMAIL    : [RTTLLHTTPSessionManager userEmail],
                                 @"_method"         : @"put"
                                 };
    
    NSMutableDictionary *combinedAttributes = [NSMutableDictionary dictionaryWithDictionary:parameters];
    if (info) {
        [combinedAttributes addEntriesFromDictionary:info];
    }
    
    NSURLSessionDataTask *task = [self POST:ITEMS_WITH_ID(@(itemId)) parameters:combinedAttributes constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        if (image) {
            [formData appendPartWithFileData:UIImageJPEGRepresentation(image, 0.6)
                                        name:@"image_url"
                                    fileName:@"imageItem.jpg"
                                    mimeType:@"image/jpeg"];
            }
        } success:^(NSURLSessionDataTask *task, id responseObject) {
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            completion(nil);
        });
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            completion(error);
        });
    }];
    
    return task;

}

// Items [/items/{id}/?{authentication_token, authentication_email}]
// Delete an item [DELETE]

- (NSURLSessionDataTask *)deleteItemWithId:(NSInteger)itemId completion:( void (^)(NSError *error))completion
{
    NSDictionary *parameters = @{AUTH_TOKEN_KEY  : [RTTLLHTTPSessionManager userToken],
                                 AUTH_USER_EMAIL : [RTTLLHTTPSessionManager userEmail]};
    
    NSURLSessionDataTask *task = [self DELETE:ITEMS_WITH_ID(@(itemId)) parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject)
                                  {
                                      dispatch_async(dispatch_get_main_queue(), ^{
                                          
                                          completion(nil);
                                          
                                      });
                                  } failure:^(NSURLSessionDataTask *task, NSError *error) {
                                      dispatch_async(dispatch_get_main_queue(), ^{
                                          completion(error);
                                      });
                                  }];
    
    return task;
}


#pragma mark CATEGORIES

//  Categories Collection [/categories?{authentication_token, authentication_email}]
//  List all Categories [GET]

- (NSURLSessionDataTask *)categoriesCompletion:( void (^)(NSArray *results, NSError *error) )completion
{
    __block JSONModelArray* categories = [self.cacheData objectForKey:@"categories"];
    
    if ([categories count] && completion) {
        dispatch_async(dispatch_get_main_queue(), ^{
            completion([categories toArray], nil);
        });
    } else {
            NSDictionary *parameters = @{AUTH_TOKEN_KEY     : [RTTLLHTTPSessionManager userToken],
                                         AUTH_USER_EMAIL    : [RTTLLHTTPSessionManager userEmail]};
        
            NSURLSessionDataTask *task = [self GET:CATEGORIES parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                categories = [[JSONModelArray alloc] initWithArray:responseObject[CATEGORIES] modelClass:[RTTLLCategory class]];
                [self.cacheData setObject:categories forKey:CATEGORIES];
                if (completion) {
                    completion([categories toArray], nil);
                }
            });
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (completion) {
                    completion(nil, error);
                }
            });
        }];
        return task;
    }
    return nil;
}

#pragma mark STATS

//  Stast [/stats?{authentication_token, authentication_email}]
//  List of statistics [GET]

- (NSURLSessionDataTask *)statsCompletion:( void (^)(RTTLLStats *stats, NSError *error) )completion
{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];

    NSDictionary *parameters = @{AUTH_TOKEN_KEY     : [RTTLLHTTPSessionManager userToken],
                                 AUTH_USER_EMAIL    : [RTTLLHTTPSessionManager userEmail]};
        
        NSURLSessionDataTask *task = [self GET:STATS parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                NSError *error = nil;
                RTTLLStats *stats = nil;
                if (responseObject) {
                    stats = [[RTTLLStats alloc] initWithDictionary:responseObject[STATS] error:&error];
                }
                if (completion) {
                    completion(stats, error);
                }

            });
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (completion) {
                    completion(nil, error);
                }
            });
        }];
        return task;
}


#pragma mark GENERIC URL CONTENT

- (void)contentFromURL:(NSURL*)url
      complentionBlock:(void (^)(NSString *content, NSError *error))block
{
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc]initWithRequest:request];
    [operation  setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        block(operation.responseString, nil);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        block(nil, error);
    }];
    
    [operation start];
}
@end
