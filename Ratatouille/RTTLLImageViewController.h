//
//  RTTLLImageViewController.h
//  Ratatouille
//
//  Created by Giorgia Marenda on 3/1/14.
//  Copyright (c) 2014 Giorgia Marenda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RTTLLImageViewController : UIViewController<UIScrollViewDelegate>

@property(strong, nonatomic) UIImage                *image;

@property (strong, nonatomic) IBOutlet UIButton     *doneButton;
@property(strong, nonatomic) IBOutlet UIScrollView  *scrollView;
@property(strong, nonatomic) IBOutlet UIImageView   *imageView;

@end
