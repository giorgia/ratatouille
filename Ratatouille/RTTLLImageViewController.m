//
//  RTTLLImageViewController.m
//  Ratatouille
//
//  Created by Giorgia Marenda on 3/1/14.
//  Copyright (c) 2014 Giorgia Marenda. All rights reserved.
//

#import "RTTLLImageViewController.h"

@interface RTTLLImageViewController ()

@end

@implementation RTTLLImageViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.doneButton.layer.borderColor = COLOR_TORTORA.CGColor;
    self.doneButton.layer.borderWidth = 3;
}

- (void) viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	self.imageView.image = self.image;
}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
	self.imageView.center = CGPointMake(CGRectGetMidX(self.scrollView.frame), CGRectGetMidY(self.scrollView.frame));
}

#pragma mark - Scroll & Zoom

- (UIView *) viewForZoomingInScrollView:(UIScrollView *)scrollView {
	return self.imageView;
}

- (void) scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
	if (scrollView.zoomScale > 1.0) {
		return;
	}
	
	if (fabsf(scrollView.contentOffset.y) < 50.0f) {
		return;
	}
	
	[self performSegueWithIdentifier:@"ExitSegue" sender:self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
