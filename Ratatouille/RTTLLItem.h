//
//  RTTLLItem.h
//  Ratatouille
//
//  Created by Giorgia Marenda on 11/3/13.
//  Copyright (c) 2013 Giorgia Marenda. All rights reserved.
//

#import "JSONModel.h"
#import "RTTLLCategory.h"

#define ITEMS_KEY               @"items"
#define ITEM_KEY                @"item"

@class RTTLLUser;

@protocol RTTLLItem @end

@interface RTTLLItem : JSONModel

@property (assign, nonatomic) int                             id;
@property (strong, nonatomic) NSString<Optional>              *name;
@property (strong, nonatomic) NSString<Optional>              *desc;
@property (strong, nonatomic) NSString<Optional>              *category_id;
@property (strong, nonatomic) NSString<Optional>              *expiration_date;
@property (strong, nonatomic) NSString<Optional>              *due_date;
@property (strong, nonatomic) NSString<Optional>              *image_url;
@property (assign, nonatomic) BOOL                            is_taken;
@property (strong, nonatomic) NSString<Optional>              *distance;

@property (strong, nonatomic) RTTLLUser<Optional>             *user;

- (NSDate *)dueDate;
- (NSDate *)expirationDate;
- (NSString *)stringdueDate;
- (NSString *)stringExpiration;

+ (NSString *)dueDateFromDate:(NSDate*)dueDate;
+ (NSString *)expirationDateFromDate:(NSDate*)expirationDate;

@end
