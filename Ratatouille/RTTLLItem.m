//
//  RTTLLItem.m
//  Ratatouille
//
//  Created by Giorgia Marenda on 11/3/13.
//  Copyright (c) 2013 Giorgia Marenda. All rights reserved.
//

#import "RTTLLItem.h"

@implementation RTTLLItem

-(NSString<Optional> *)distance
{
    if(_distance && _distance.length) {
        CGFloat dist = [_distance integerValue];
        if(dist < 1000) {
            return [NSString stringWithFormat:@"%.0f M", dist];
        }
        dist = dist / 1000;
        return [NSString stringWithFormat:@"%.01f KM", dist];
    }
    
    return @"";
}

- (NSDate *)dueDate
{
    if (_due_date) {
        NSDateFormatter *dueformatter = [[NSDateFormatter alloc] init];
        [dueformatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
        NSDate *dueDate = [dueformatter dateFromString:_due_date];
    
        return dueDate;
    }
    return [NSDate date];
}

- (NSDate *)expirationDate
{
    if (_expiration_date) {
        NSDateFormatter *expformatter = [[NSDateFormatter alloc] init];
        [expformatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
        NSDate *expDate = [expformatter dateFromString:_expiration_date];
    
        return expDate;
    }
    return [NSDate date];
}

+ (NSString *)dueDateFromDate:(NSDate*)dueDate
{
    NSDateFormatter *dueformatter = [[NSDateFormatter alloc] init];
    [dueformatter setDateFormat:@"yyyy/MM/dd  HH:mm"];
    NSString *dueDateStr = [dueformatter stringFromDate:dueDate];
    
    return dueDateStr;
}

+ (NSString *)expirationDateFromDate:(NSDate*)expirationDate
{
    NSDateFormatter *expformatter = [[NSDateFormatter alloc] init];
    [expformatter setDateFormat:@"yyyy/MM/dd"];
    NSString *expDate = [expformatter stringFromDate:expirationDate];
    
    return expDate;
}

- (NSString *)stringdueDate
{
    NSDateFormatter *dueformatter = [[NSDateFormatter alloc] init];
    [dueformatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
    NSString *dueDateStr = [dueformatter stringFromDate:[self expirationDate]];
    
    return dueDateStr;
}

- (NSString *)stringExpiration
{
    NSDateFormatter *expformatter = [[NSDateFormatter alloc] init];
    [expformatter setDateFormat:@"yyyy-MM-dd'T'hh:mm:ss'Z'"];
    NSString *expDate = [expformatter stringFromDate:[self dueDate]];
    
    return expDate;
}

@end
