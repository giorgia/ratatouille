//
//  RTTLLItemViewCell.h
//  Ratatouille
//
//  Created by Giorgia Marenda on 11/1/13.
//  Copyright (c) 2013 Giorgia Marenda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RTTLLItemViewCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIImageView  *imageView;
@property (strong, nonatomic) IBOutlet UILabel      *nameLabel;

- (void)setImageFromUrl:(NSString*)URLString;

@end
