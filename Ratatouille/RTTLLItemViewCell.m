//
//  RTTLLItemViewCell.m
//  Ratatouille
//
//  Created by Giorgia Marenda on 11/1/13.
//  Copyright (c) 2013 Giorgia Marenda. All rights reserved.
//

#import "RTTLLItemViewCell.h"
#import "UIImageView+AFNetworking.h"

@implementation RTTLLItemViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.nameLabel.font = LATO_REG(10);
}

- (void)setImageFromUrl:(NSString*)URLString
{
    __block typeof(self) bself  = self;
    NSURLRequest *request       = [NSURLRequest requestWithURL:[NSURL URLWithString:URLString]];
    UIImage *placeholder        = [UIImage imageNamed:@"empty_category_fridge"];
    
    [self.imageView setImageWithURLRequest:request placeholderImage:placeholder success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
     {
         bself.imageView.image      = image;
         bself.imageView.alpha      = 0.0;
         [UIView animateWithDuration:1.0 animations:^{
             bself.imageView.alpha = 1.0;
         }];
         
     } failure:nil];
}


@end
