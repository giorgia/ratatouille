//
//  RTTLLItemViewController.h
//  Ratatouille
//
//  Created by Giorgia Marenda on 11/1/13.
//  Copyright (c) 2013 Giorgia Marenda. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RTTLLItem;
@class RTTLLUser;

@protocol RTTLLItemDelegate;

@interface RTTLLItemViewController : UITableViewController

@property (nonatomic, assign)       id <RTTLLItemDelegate>  delegate;

@property (nonatomic, strong) RTTLLUser *user;
@property (nonatomic, strong) RTTLLItem *item;
@property (nonatomic, assign) BOOL      isMine;
@property (nonatomic, assign) BOOL      editMode;

@property (strong, nonatomic) NSDate    *dueDate;
@property (strong, nonatomic) NSDate    *expirationDate;

- (void)setEditble:(BOOL)editable;
- (void)markAsTaken;
- (void)deleteItem;
- (void)donePressed;

@end

@protocol RTTLLItemDelegate <NSObject>

- (void)didUpdateItemViewController:(RTTLLItemViewController*)itemController;
- (void)itemViewController:(RTTLLItemViewController*)itemController showDatePickerWithDate:(NSDate*)date withType:(UIDatePickerMode)mode;

@end