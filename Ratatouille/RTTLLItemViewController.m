//
//  RTTLLItemViewController.m
//  Ratatouille
//
//  Created by Giorgia Marenda on 11/1/13.
//  Copyright (c) 2013 Giorgia Marenda. All rights reserved.
//

#import "RTTLLCategoriesViewController.h"
#import "RTTLLImageViewController.h"
#import "RTTLLItemViewController.h"
#import "UIImageView+AFNetworking.h"
#import "RTTLLHTTPSessionManager.h"
#import "UIImage+Additions.h"
#import "RTTLLCategory.h"
#import "NSString+MD5.h"
#import "RTTLLItem.h"

@interface RTTLLItemViewController () <UIPickerViewDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate, RTTLLCategoryChooserDelegate>

@property (nonatomic, assign) BOOL      isNewItem;

@property (nonatomic, assign) BOOL      imageUploaded;
@property (nonatomic, assign) BOOL      isDonePressed;


@property (strong, nonatomic) IBOutlet UIImageView                  *itemImageView;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView      *imageActivityIndicator;
@property (strong, nonatomic) IBOutlet UITextField                  *titleTextField;
@property (strong, nonatomic) IBOutlet UITextView                   *descriptionTextView;
@property (strong, nonatomic) IBOutlet UIImageView                  *categoryImage;
@property (strong, nonatomic) IBOutlet UILabel                      *categoryLabel;
@property (strong, nonatomic) IBOutlet UILabel                      *expLabel;
@property (strong, nonatomic) IBOutlet UIButton                     *expButton;
@property (strong, nonatomic) IBOutlet UILabel                      *dueLabel;
@property (strong, nonatomic) IBOutlet UIButton                     *dueButton;
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray   *titleLabels;
@property (strong, nonatomic) UIImagePickerController               *imagePickerController;

@property (strong, nonatomic) RTTLLCategory                         *category;


@property (strong, nonatomic) IBOutlet UIButton *editImageButton;
@property (strong, nonatomic) IBOutlet UIButton *addImageButton;

@end

@implementation RTTLLItemViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    for (UILabel *title in self.titleLabels) {
        title.font = LATO_LIGHT(14);
    }
    self.titleTextField.font            = LATO_REG(20);
    self.addImageButton.titleLabel.font = LATO_REG(10);
    self.descriptionTextView.font       = LATO_LIGHT(15);
    self.categoryLabel.font             = LATO_LIGHT(15);
    self.expLabel.font                  = LATO_LIGHT(22);
    self.dueLabel.font                  = LATO_LIGHT(22);
    
    self.isNewItem = !self.item;
    
    [self setEditble: self.isMine ? self.isNewItem : NO];
    
    if (!self.isNewItem) {
        [self reloadData];
    } else {
        self.dueDate        = self.expirationDate = [NSDate date];
        self.dueLabel.text  = [RTTLLItem dueDateFromDate:[NSDate date]];
        self.expLabel.text  = [RTTLLItem expirationDateFromDate:[NSDate date]];
    }
}

-(void)setExpirationDate:(NSDate *)expirationDate
{
    _expirationDate     = expirationDate;
    self.expLabel.text  = [RTTLLItem expirationDateFromDate:expirationDate];
}

-(void)setDueDate:(NSDate *)dueDate
{
    _dueDate            = dueDate;
    self.dueLabel.text  = [RTTLLItem dueDateFromDate:dueDate];
}

- (void)donePressed
{
    self.isDonePressed = YES;
    [self putItem];
}

- (void)setEditble:(BOOL)editable
{
    self.editMode                   = editable;
    self.editImageButton.hidden     = !editable || self.isNewItem;
    self.addImageButton.hidden      = !editable || !self.isNewItem;
    self.titleTextField.enabled     = editable;
    self.descriptionTextView.editable = editable;
    self.dueButton.hidden           = !editable;
    self.expButton.hidden           = !editable;
    [self.titleTextField becomeFirstResponder];
}


- (IBAction)getPhoto:(id)sender
{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    
    [imagePicker setSourceType:UIImagePickerControllerSourceTypeSavedPhotosAlbum];
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        [self showImagePickerForSourceType:UIImagePickerControllerSourceTypeCamera];
        
    } else {
        
        [self showImagePickerForSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    }
}

- (IBAction)changeExpirationDate:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(itemViewController:showDatePickerWithDate:withType:)]) {
        [self.delegate itemViewController:self showDatePickerWithDate:[self.item expirationDate] withType:UIDatePickerModeDate];
    }
}

- (IBAction)changeDueDate:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(itemViewController:showDatePickerWithDate:withType:)]) {
        [self.delegate itemViewController:self showDatePickerWithDate:[self.item dueDate] withType:UIDatePickerModeDateAndTime];
    }
}

#pragma mark -
#pragma mark Item Operations

- (void)postNewItem
{
    self.editImageButton.hidden = YES;
    [self.imageActivityIndicator startAnimating];
    __block typeof(self) bself = self;
    
    UIImage *image = [self.itemImageView.image resizedImage:CGSizeMake(640, 640) drawTransposed:YES interpolationQuality:kCGInterpolationMedium];
    
    [[RTTLLHTTPSessionManager sharedManager] createItemWithImage:image completion:^(RTTLLItem* item, NSError *error)
     {
         [self.imageActivityIndicator stopAnimating];
         
         if (error) {
             [SVProgressHUD showErrorWithStatus:[error errorDescription]];
             return;
         }
         self.editImageButton.hidden    = NO;
         self.isNewItem                 = NO;
         bself.item                     = item;
         bself.imageUploaded            = YES;
         if (bself.isDonePressed) {
             [self putItem];
         }
     }];
}

- (void)putImage
{
    self.editImageButton.hidden = YES;
    [self.imageActivityIndicator startAnimating];
    
    __block typeof(self) bself = self;
    
    UIImage *image = [self.itemImageView.image resizedImage:CGSizeMake(640, 640) drawTransposed:YES interpolationQuality:kCGInterpolationMedium];
    
    [[RTTLLHTTPSessionManager sharedManager] updateItemWithId:self.item.id withInfo:nil andImage:image completion:^(NSError *error)
     {
         [self.imageActivityIndicator stopAnimating];
         [SVProgressHUD dismiss];

         if (error) {
             [SVProgressHUD showErrorWithStatus:[error errorDescription]];
             return;
         }
         self.editImageButton.hidden = NO;
         
         bself.imageUploaded = YES;
         if (bself.isDonePressed) {
             [self putItem];
         }
     }];
}

- (void)putItem
{
    
    if([self isValidForm]) {
        
        NSDictionary *info = @{  @"name": self.titleTextField.text,
                                 @"desc": self.descriptionTextView.text,
                                 @"due_date": self.dueDate,
                                 @"expiration_date": self.expirationDate,
                                 @"category_id": [@(self.category.id) stringValue]
                                 };
        [self putItemWithInfo:info];
    }
}

- (void)putItemWithInfo:(NSDictionary*)info
{
    [SVProgressHUD showWithStatus:@"Uploading..."];
    
    [[RTTLLHTTPSessionManager sharedManager] updateItemWithId:self.item.id withInfo:info andImage:nil completion:^(NSError *error)
     {
         [SVProgressHUD dismiss];
         if (error) {
             [SVProgressHUD showErrorWithStatus:[error errorDescription]];
             return;
         }
         if ([self.delegate respondsToSelector:@selector(didUpdateItemViewController:)]) {
             [self.delegate didUpdateItemViewController:self];
         }
     }];
}

- (void)deleteItem
{
    [SVProgressHUD showWithStatus:@"Deleting..."];

    [[RTTLLHTTPSessionManager sharedManager] deleteItemWithId:self.item.id completion:^(NSError *error) {
        [SVProgressHUD dismiss];
        if (error) {
            [SVProgressHUD showErrorWithStatus:[error errorDescription]];
            return;
        }
        if ([self.delegate respondsToSelector:@selector(didUpdateItemViewController:)]) {
            [self.delegate didUpdateItemViewController:self];
        }
    }];
}

- (void)markAsTaken
{
    NSDictionary *info = @{  @"is_taken": [@(YES) stringValue] };
    
    [self putItemWithInfo:info];
}

- (BOOL)isValidForm
{
    if(!self.titleTextField.text.length) {
        [SVProgressHUD showErrorWithStatus:@"Fill the title, please!"];
        return NO;
    }
    
    if (!self.itemImageView.image) {
        [SVProgressHUD showErrorWithStatus:@"Take a photo of the item, please!"];
        return NO;
    }
    
    if (!self.category) {
        [SVProgressHUD showErrorWithStatus:@"Choose a category, please!"];
        return NO;
    }
    
    if(!self.dueDate) {
        [SVProgressHUD showErrorWithStatus:@"Fill due date, please!"];
        return NO;
    }

    if(!self.expirationDate) {
        [SVProgressHUD showErrorWithStatus:@"Fill expiration date, please!"];
        return NO;
    }
    
    if(self.isNewItem && (!self.imageUploaded || !self.isDonePressed)) {
        return NO;
    }
    
    return YES;
}

#pragma mark -
#pragma mark Data

- (void)reloadData
{
    self.imageActivityIndicator.hidden = NO;
    [self.imageActivityIndicator startAnimating];
    
    __block typeof(self) bself = self;
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:self.item.image_url]];
    [self.itemImageView setImageWithURLRequest:request placeholderImage:nil success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
     {
         [bself.imageActivityIndicator stopAnimating];
         bself.itemImageView.image  = image;
         bself.itemImageView.alpha  = 0.0;
         [UIView animateWithDuration:0.3 animations:^{
             bself.itemImageView.alpha = 1.0;
         }];
         
     } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error)
     {
         [bself.imageActivityIndicator stopAnimating];
     }];
    
    self.titleTextField.text        = [self.item.name uppercaseString];
    self.descriptionTextView.text   = self.item.desc;
    self.expLabel.text              = [RTTLLItem expirationDateFromDate:[self.item expirationDate]];
    self.dueLabel.text              = [RTTLLItem dueDateFromDate:[self.item dueDate]];
    self.expirationDate             = [self.item expirationDate];
    self.dueDate                    = [self.item dueDate];
    
    self.categoryImage.layer.cornerRadius = self.categoryImage.frame.size.width / 2;

    
    self.category = [RTTLLCategory categoryWithId:self.item.category_id];
    if (!self.category) {
        [RTTLLCategory categoryWithId:self.item.category_id complention:^(RTTLLCategory *category, NSError *error) {
            bself.category = category;
            [bself reloadCategory];
        }];
    }
    [self reloadCategory];
}

- (void)reloadCategory
{
    [self.categoryImage setImageWithURL:[NSURL URLWithString:self.category.image_url] placeholderImage:[UIImage imageNamed:@"empty_category_add-item"]];
    self.categoryLabel.text = [self.category.name uppercaseString];
    [self.tableView reloadData];
}

#pragma mark -
#pragma mark Navigation

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    return self.editMode || [identifier isEqualToString:@"image"];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue destinationViewController] isKindOfClass:[RTTLLCategoriesViewController class]]) {
        // Get the new view controller using [segue destinationViewController].
        RTTLLCategoriesViewController *chooser = (RTTLLCategoriesViewController*)[segue destinationViewController];
        chooser.delegate = self;
    } else {
        RTTLLImageViewController *imageController = (RTTLLImageViewController *)segue.destinationViewController;
        // Pass the selected object to the new view controller.
        imageController.image = self.itemImageView.image;
    }
}

- (IBAction) unwindFromSegue:(UIStoryboardSegue *)segue
{
	
}

- (UIStoryboardSegue *)segueForUnwindingToViewController:(UIViewController *)toViewController fromViewController:(UIViewController *)fromViewController identifier:(NSString *)identifier
{
	UIStoryboardSegue *imageTransition = [[UIStoryboardSegue alloc] initWithIdentifier:identifier source:fromViewController destination:toViewController];
		
	return imageTransition;
}

#pragma mark -
#pragma mark UIImagePickerController

- (void)showImagePickerForSourceType:(UIImagePickerControllerSourceType)sourceType
{
    UIImagePickerController *imagePickerController      = [[UIImagePickerController alloc] init];
    imagePickerController.modalPresentationStyle        = UIModalPresentationCurrentContext;
    imagePickerController.allowsEditing                 = YES;
    imagePickerController.sourceType                    = sourceType;
    imagePickerController.delegate                      = self;
    imagePickerController.navigationBar.barTintColor    = COLOR_TORTORA;
    
    self.imagePickerController                          = imagePickerController;
    [self presentViewController:self.imagePickerController animated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [picker dismissViewControllerAnimated:YES completion:NULL];
    picker.allowsEditing            = YES;
    picker.modalPresentationStyle   = UIModalPresentationCurrentContext;
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    self.itemImageView.image            = chosenImage;
    self.addImageButton.hidden          = YES;
    self.editImageButton.hidden         = NO;
    self.imageActivityIndicator.hidden  = NO;
    if (self.isNewItem) {
        [self postNewItem];
    } else {
        [self putImage];
    }
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark -
#pragma mark RTTLLCategoryChooserDelegate

-(void)dismissChooserWithSelectedCategory:(RTTLLCategory *)category
{
    self.category = category;
    [self reloadCategory];
}


@end
