//
//  RTTLLLocationManager.h
//  Ratatouille
//
//  Created by Giorgia Marenda on 11/8/13.
//  Copyright (c) 2013 Giorgia Marenda. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>
#import <Foundation/Foundation.h>

@interface RTTLLLocationManager : CLLocationManager

@property (nonatomic, strong) CLLocation* currentlocation;

+ (RTTLLLocationManager *)locationManager;

@end
