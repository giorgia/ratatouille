//
//  RTTLLLocationManager.m
//  Ratatouille
//
//  Created by Giorgia Marenda on 11/8/13.
//  Copyright (c) 2013 Giorgia Marenda. All rights reserved.
//


#import "RTTLLLocationManager.h"

@interface RTTLLLocationManager()<CLLocationManagerDelegate>

@property CLLocationDistance distanceMoved;

@end

@implementation RTTLLLocationManager


- (id)init
{
    self = [super init];
    if (self != nil) {
        self.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
        self.delegate = self;
    }
    return self;
}

#pragma mark - Singleton

+ (RTTLLLocationManager *)locationManager
{
    static RTTLLLocationManager *locationManager = nil;
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        locationManager = [[self alloc] init];
    });
    return locationManager;
}

- (void)setCurrentLocation:(CLLocation *)currentLocation
{
    _currentlocation = currentLocation;
    self.distanceMoved = 0.0;
}

-(void) locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    NSTimeInterval age = -[newLocation.timestamp timeIntervalSinceNow];
    
    if (age > 120) return;    // ignore old (cached) updates

    if (newLocation.horizontalAccuracy < 0) return;   // ignore invalid udpates
    
    // EDIT: need a valid oldLocation to be able to compute distance
    if (self.location == nil || self.location.horizontalAccuracy < 0) {
        self.currentLocation = newLocation;
        return;
    }
    
    CLLocationDistance distance = [newLocation distanceFromLocation:self.location];
    self.distanceMoved = distance;
    
    if (self.distanceMoved >= 50.0) {
        UILocalNotification *notification = [[UILocalNotification alloc] init];
        NSDate *now = [NSDate date];
        NSDate *dateToFire = [now dateByAddingTimeInterval:2];
        notification.fireDate = dateToFire;
        
        notification.alertBody = [NSString stringWithFormat:@"Ti sei spostato di %f m", self.distanceMoved];
        [[UIApplication sharedApplication] scheduleLocalNotification:notification];
    }
    
    NSLog(@"Distance %f location <%f, %f>", self.distanceMoved, self.location.coordinate.latitude, self.location.coordinate.longitude);

}

@end
