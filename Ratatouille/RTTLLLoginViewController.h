//
//  RTTLLLoginViewController.h
//  Ratatouille
//
//  Created by Giorgia Marenda on 11/23/13.
//  Copyright (c) 2013 Giorgia Marenda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RTTLLLoginViewController : UITableViewController

@property (strong, nonatomic) IBOutlet UITextField  *emailTextField;
@property (strong, nonatomic) IBOutlet UITextField  *passwordTextField;
@property (strong, nonatomic) IBOutlet UIButton     *signinButton;
@property (strong, nonatomic) IBOutlet UIButton     *accountButton;
@property (strong, nonatomic) IBOutlet UIButton     *forgotButton;

@end
