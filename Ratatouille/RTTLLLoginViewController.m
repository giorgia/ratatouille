//
//  RTTLLLoginViewController.m
//  Ratatouille
//
//  Created by Giorgia Marenda on 11/23/13.
//  Copyright (c) 2013 Giorgia Marenda. All rights reserved.
//

#import "RTTLLLoginViewController.h"
#import "RTTLLHTTPSessionManager.h"
#import "RTTLLAppDelegate.h"

@interface RTTLLLoginViewController ()

@end

@implementation RTTLLLoginViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.signinButton.titleLabel.font   = LATO_REG(13);
    self.accountButton.titleLabel.font  = LATO_REG(13);
    self.forgotButton.titleLabel.font   = LATO_REG(13);
}

- (IBAction)signUpTapped:(id)sender
{
    [SVProgressHUD show];
    [self.passwordTextField resignFirstResponder];
    [self postSignIn];
}

- (void)postSignIn
{
    NSDictionary *info = @{ @"email": self.emailTextField.text,
                            @"password": self.passwordTextField.text,
                            };
    
    [[RTTLLHTTPSessionManager sharedManager] signInWithInfo:info completion:^(NSError *error)
     {
         if (error) {
             [SVProgressHUD showErrorWithStatus:[error errorDescription]];
         } else {
             RTTLLAppDelegate *delegate = (RTTLLAppDelegate *)[[UIApplication sharedApplication] delegate];
             [delegate loadStoryboardAnimated:YES];
         }
     }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
