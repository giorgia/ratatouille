//
//  RTTLLMagnetBehavior.h
//  Ratatouille
//
//  Created by Giorgia Marenda on 12/18/13.
//  Copyright (c) 2013 Giorgia Marenda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RTTLLMagnetBehavior : UIDynamicBehavior

- (void)addItem:(id <UIDynamicItem>)item;

@end
