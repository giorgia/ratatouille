//
//  RTTLLMagnetBehavior.m
//  Ratatouille
//
//  Created by Giorgia Marenda on 12/18/13.
//  Copyright (c) 2013 Giorgia Marenda. All rights reserved.
//

#import "RTTLLMagnetBehavior.h"

@interface RTTLLMagnetBehavior ()
@property (strong, nonatomic) UICollisionBehavior *collider;
@end

@implementation RTTLLMagnetBehavior


- (UICollisionBehavior *)collider
{
    if (!_collider) {
        _collider = [[UICollisionBehavior alloc] init];
        [_collider setCollisionMode:UICollisionBehaviorModeEverything];
        _collider.translatesReferenceBoundsIntoBoundary = YES;
    }
    return _collider;
}

-(void)addItem:(id<UIDynamicItem>)item
{
    [self.collider addItem:item];
}

-(instancetype)init
{
    self = [super init];
    [self addChildBehavior:self.collider];
    return self;
}

@end
