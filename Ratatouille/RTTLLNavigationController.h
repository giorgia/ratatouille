//
//  RTTLLNavigationController.h
//  Ratatouille
//
//  Created by Giorgia Marenda on 2/7/14.
//  Copyright (c) 2014 Giorgia Marenda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RTTLLNavigationController : UINavigationController

@end
