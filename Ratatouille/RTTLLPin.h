//
//  RTTLLPin.h
//  Ratatouille
//
//  Created by Giorgia Marenda on 11/3/13.
//  Copyright (c) 2013 Giorgia Marenda. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface RTTLLPin : NSObject <MKAnnotation>

@property (nonatomic, assign)   CLLocationCoordinate2D  coordinate;
@property (nonatomic, copy)     NSString                *title;
@property (nonatomic, copy)     NSString                *subtitle;
@property (nonatomic, assign)   NSInteger               userId;

@end
