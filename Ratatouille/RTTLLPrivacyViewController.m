//
//  RTTLLPrivacyViewController.m
//  Ratatouille
//
//  Created by Giorgia Marenda on 2/7/14.
//  Copyright (c) 2014 Giorgia Marenda. All rights reserved.
//
#import <MessageUI/MFMailComposeViewController.h>
#import <StoreKit/StoreKit.h>
#import "RTTLLPrivacyViewController.h"
#import "RTTLLWebViewViewController.h"

typedef enum {
    RTTLLAboutCellTypeTutorial = 0,
    RTTLLAboutCellTypePrivacy,
    RTTLLAboutCellTypeTermOfService,
    RTTLLAboutCellTypeRate,
    RTTLLAboutCellTypeSupport
} RTTLLAboutCellType;

@interface RTTLLPrivacyViewController ()<MFMailComposeViewControllerDelegate, SKStoreProductViewControllerDelegate>

@property (strong, nonatomic) IBOutlet UIBarButtonItem              *closeButton;
@property (strong, nonatomic) IBOutlet UITextView                   *textView;
@property (strong, nonatomic) IBOutlet UILabel                      *titleLabel;
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray   *labels;

@end

@implementation RTTLLPrivacyViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self.closeButton setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor whiteColor], NSFontAttributeName:LATO_BOLD(12)} forState:UIControlStateNormal];

    self.titleLabel.font    = LATO_BOLD(13);
    self.textView.font      = LATO_REG(14);
    for (UILabel *label in self.labels) {
        label.font = LATO_REG(13);
    }
}


- (void)support
{
    if ([MFMailComposeViewController canSendMail])
    {
        // The device can send email.
        MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
        picker.mailComposeDelegate          = self;
        picker.navigationBar.tintColor      = [UIColor whiteColor];
        [[picker navigationBar] setTintColor:COLOR_LOBSTER];
        [picker setSubject:@"I need support"];
        
        // Set up recipients
        NSArray *toRecipients = [NSArray arrayWithObject:@"info@ratatouille-app.com"];
        [picker setToRecipients:toRecipients];
        
        [self presentViewController:picker animated:YES completion:NULL];
    }
    else
    {
        // The device can not send email.
        [SVProgressHUD showErrorWithStatus:@"Device not configured to send mail."];
    }
}

- (void)rate
{
    NSDictionary *appParameters = [NSDictionary dictionaryWithObject:APP_STORE_ID
                                                              forKey:SKStoreProductParameterITunesItemIdentifier];
    
    SKStoreProductViewController *productViewController = [[SKStoreProductViewController alloc] init];
    [productViewController setDelegate:self];
    [productViewController loadProductWithParameters:appParameters
                                          completionBlock:^(BOOL result, NSError *error)
     {
         if (error) {
             [SVProgressHUD showErrorWithStatus:[error localizedDescription]];
         } else {
             // Present Store Product View Controller
             [self presentViewController:productViewController animated:YES completion:nil];
         }
     }];
}

- (void)openWebviewWithURL:(NSString*)url andTitle:(NSString*)title
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:
                                @"Main" bundle:[NSBundle mainBundle]];
    RTTLLWebViewViewController *controller  = [storyboard instantiateViewControllerWithIdentifier:@"webviewController"];
    controller.urlString                    = url;
    controller.title                        = title;
    [self.navigationController pushViewController:controller animated:YES];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];

    switch (cell.tag) {
        case RTTLLAboutCellTypePrivacy:
        {
            [self openWebviewWithURL:PRIVACY_URL andTitle:@"PRIVACY POLICY"];
            break;
        }
        case RTTLLAboutCellTypeTermOfService:
        {
            [self openWebviewWithURL:TERM_OF_SERVICE_URL andTitle:@"TERMS OF SERVICE"];
            break;
        }
        case RTTLLAboutCellTypeRate:
        {
            [self rate];
            break;
        }
        case RTTLLAboutCellTypeSupport:
        {
            [self support];
            break;
        }
        default:
            break;
    }
}


#pragma mark - Mail Delegate Methods

// -------------------------------------------------------------------------------
//  mailComposeController:didFinishWithResult:
//  Dismisses the email composition interface when users tap Cancel or Send.
//  Proceeds to update the message field with the result of the operation.
// -------------------------------------------------------------------------------
- (void)mailComposeController:(MFMailComposeViewController*)controller
          didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    NSString *message = @"";
    // Notifies users about errors associated with the interface
    switch (result)
    {
        case MFMailComposeResultCancelled:
            message = @"Result: Mail sending canceled";
            [SVProgressHUD showSuccessWithStatus:message];
            break;
        case MFMailComposeResultSaved:
            message = @"Result: Mail saved";
            [SVProgressHUD showSuccessWithStatus:message];
            break;
        case MFMailComposeResultSent:
            message = @"Result: Mail sent";
            [SVProgressHUD showErrorWithStatus:message];
            break;
        case MFMailComposeResultFailed:
            message = @"Result: Mail sending failed";
            [SVProgressHUD showErrorWithStatus:message];
            break;
        default:
            message = @"Result: Mail not sent";
            [SVProgressHUD showErrorWithStatus:message];
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark - Store Delegate Methods

-(void)productViewControllerDidFinish:(SKStoreProductViewController *)viewController
{
    [viewController dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onClose:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
