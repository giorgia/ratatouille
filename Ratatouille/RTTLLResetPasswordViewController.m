//
//  RTTLLResetPasswordViewController.m
//  Ratatouille
//
//  Created by Giorgia Marenda on 3/15/14.
//  Copyright (c) 2014 Giorgia Marenda. All rights reserved.
//

#import "RTTLLResetPasswordViewController.h"
#import "RTTLLHTTPSessionManager.h"

@interface RTTLLResetPasswordViewController ()<UIWebViewDelegate>

@property (strong, nonatomic) IBOutlet UIWebView        *webView;
@property (strong, nonatomic) IBOutlet UIBarButtonItem  *closeButton;

@end

@implementation RTTLLResetPasswordViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.webView.scrollView.scrollEnabled = NO;
    [self.closeButton setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor whiteColor], NSFontAttributeName:LATO_BOLD(12)} forState:UIControlStateNormal];
    
    [SVProgressHUD showWithStatus:@"Loading..."];
    
    __block typeof(self) bself = self;
    [[RTTLLHTTPSessionManager sharedManager] contentFromURL:[NSURL URLWithString:FORGOT_PWD_URL] complentionBlock:^(NSString *content, NSError *error) {
        [bself.webView loadHTMLString:content baseURL:[NSURL URLWithString:BASE_URL]];
    }];
}

-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    [SVProgressHUD dismiss];
}

- (IBAction)onClose:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
