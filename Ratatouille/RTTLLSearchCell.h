//
//  RTTLLSearchCell.h
//  Ratatouille
//
//  Created by Giorgia Marenda on 1/31/14.
//  Copyright (c) 2014 Giorgia Marenda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RTTLLSearchCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView  *itemImageView;
@property (strong, nonatomic) IBOutlet UILabel      *nameLabel;
@property (strong, nonatomic) IBOutlet UILabel      *descLabel;
@property (strong, nonatomic) IBOutlet UILabel      *distanceLabel;

@end
