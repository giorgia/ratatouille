//
//  RTTLLSearchCell.m
//  Ratatouille
//
//  Created by Giorgia Marenda on 1/31/14.
//  Copyright (c) 2014 Giorgia Marenda. All rights reserved.
//

#import "RTTLLSearchCell.h"

@implementation RTTLLSearchCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void)awakeFromNib
{
    [super awakeFromNib];
    self.nameLabel.font     = LATO_REG(15);
    self.descLabel.font     = LATO_LIGHT(12);
    self.distanceLabel.font = LATO_LIGHT(13);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
