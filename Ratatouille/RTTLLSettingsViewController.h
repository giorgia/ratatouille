//
//  RTTLLSettingsViewController.h
//  Ratatouille
//
//  Created by Giorgia Marenda on 1/30/14.
//  Copyright (c) 2014 Giorgia Marenda. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RTTLLUser;

@interface RTTLLSettingsViewController : UITableViewController

@property (strong, nonatomic) RTTLLUser *user;

@end
