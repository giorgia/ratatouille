//
//  RTTLLSettingsViewController.m
//  Ratatouille
//
//  Created by Giorgia Marenda on 1/30/14.
//  Copyright (c) 2014 Giorgia Marenda. All rights reserved.
//

#import "RTTLLFridgeLocationViewController.h"
#import "RTTLLUpdateUserViewController.h"
#import "RTTLLSettingsViewController.h"
#import "RTTLLNavigationController.h"
#import "RTTLLHTTPSessionManager.h"
#import "RTTLLAppDelegate.h"
#import "RTTLLFridge.h"
#import "RTTLLUser.h"


@interface RTTLLSettingsViewController () <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UIBarButtonItem  *closeButton;
@property (strong, nonatomic) IBOutlet UILabel          *rangeLabel;
@property (strong, nonatomic) IBOutlet UIButton         *logoutButton;
@property (strong, nonatomic) IBOutlet UILabel          *editProfileLabel;
@property (strong, nonatomic) IBOutlet UILabel          *editPositionLabel;
@property (strong, nonatomic) IBOutlet UILabel          *changePasswordLabel;

@property (strong, nonatomic) IBOutlet UIView           *footerView;
@property (strong, nonatomic) IBOutlet UISlider         *rangeSlider;

@end

@implementation RTTLLSettingsViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.rangeLabel.font                = LATO_LIGHT(14);
    self.editProfileLabel.font          = LATO_LIGHT(14);
    self.editPositionLabel.font         = LATO_LIGHT(14);
    self.changePasswordLabel.font       = LATO_LIGHT(14);
    self.logoutButton.titleLabel.font   = LATO_REG(13);

    
    [self.closeButton setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor whiteColor], NSFontAttributeName:LATO_BOLD(12)} forState:UIControlStateNormal];

    NSString *radius = [[NSUserDefaults standardUserDefaults] objectForKey:@"radius"];
    [self.rangeSlider setValue:[radius intValue]/1000 animated:YES];
}

- (void)editFrigeLocation
{
    UIStoryboard *loginStoryboard = [UIStoryboard storyboardWithName:@"Login" bundle:nil];
        
    RTTLLFridgeLocationViewController  *fridgeViewController = [loginStoryboard instantiateViewControllerWithIdentifier:@"location"];
    CLLocationCoordinate2D location = CLLocationCoordinate2DMake([self.user.fridge.lat doubleValue], [self.user.fridge.lng doubleValue]);
    fridgeViewController.location   = [[CLLocation alloc] initWithLatitude:location.latitude longitude:location.longitude];
    [self.navigationController pushViewController:fridgeViewController animated:YES];
}

- (IBAction)logoutTapped:(id)sender
{
    [[RTTLLHTTPSessionManager sharedManager] signOutCompletion:^(NSError *error) {
        RTTLLAppDelegate *delegate = (RTTLLAppDelegate *)[[UIApplication sharedApplication] delegate];
        [delegate loadStoryboardAnimated:YES];
    }];   
}

- (IBAction)sliderChanged:(id)sender
{
    int sliderValue = ((int)((self.rangeSlider.value + 2.5) / 5) * 5);
    [self.rangeSlider setValue:sliderValue animated:YES];
    [[NSUserDefaults standardUserDefaults] setObject:[@(sliderValue*1000) stringValue] forKey:@"radius"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (IBAction)dismiss:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return self.footerView;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row) {
        case 1:
            [self editFrigeLocation];
        default:
            break;
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"profile"]) {
        RTTLLUpdateUserViewController *profileViewController = (RTTLLUpdateUserViewController*)[segue destinationViewController];
        profileViewController.user = self.user;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
