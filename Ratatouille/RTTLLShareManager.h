//
//  RTTLLShareManager.h
//  Ratatouille
//
//  Created by Giorgia Marenda on 3/29/14.
//  Copyright (c) 2014 Giorgia Marenda. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Social/Social.h>

@interface RTTLLShareManager : NSObject

+(void)shareActionFrom:(id)controller withText:(NSString*)text image:(UIImage*)image url:(NSURL*)URL;


@end
