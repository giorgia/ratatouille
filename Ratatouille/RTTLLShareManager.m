//
//  RTTLLShareManager.m
//  Ratatouille
//
//  Created by Giorgia Marenda on 3/29/14.
//  Copyright (c) 2014 Giorgia Marenda. All rights reserved.
//

#import "RTTLLShareManager.h"

@implementation RTTLLShareManager

+(void)shareActionFrom:(id)controller withText:(NSString*)text image:(UIImage*)image url:(NSURL*)URL
{
    NSArray *activityItems = @[image, URL, text];
    UIActivityViewController *activityViewControntroller = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
    activityViewControntroller.excludedActivityTypes = @[];
    
    [controller presentViewController:activityViewControntroller animated:true completion:nil];
}


+ (SLComposeViewController*)composerOfType:(NSString*)type withText:(NSString*)text image:(UIImage*)image url:(NSURL*)URL
{
    // Present the feed dialog
    if ([SLComposeViewController isAvailableForServiceType:type])
    {
        [SVProgressHUD showWithStatus:@"Wait..."];
        // Device is able to send a  message
        SLComposeViewController *composeController = [SLComposeViewController
                                                      composeViewControllerForServiceType:type];
        
        [composeController setInitialText:text];
        [composeController addImage:image];
        [composeController addURL:URL];
        
        return composeController;
        
    } else {
        NSString *message = [NSString stringWithFormat:@"Sign in with your %@ account from system settings.", type == SLServiceTypeFacebook ? @"Facebook" : @"Twitter"];
        
        [SVProgressHUD showErrorWithStatus:message];
    }
    return nil;
}

@end
