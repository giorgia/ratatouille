//
//  RTTLLSignUpViewController.h
//  Ratatouille
//
//  Created by Giorgia Marenda on 11/23/13.
//  Copyright (c) 2013 Giorgia Marenda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RTTLLSignUpViewController : UITableViewController

@property (strong, nonatomic) IBOutlet UITextField  *nameTextField;
@property (strong, nonatomic) IBOutlet UITextField  *surnameTextField;
@property (strong, nonatomic) IBOutlet UITextField  *phoneNumber;
@property (strong, nonatomic) IBOutlet UITextField  *emailTextField;
@property (strong, nonatomic) IBOutlet UITextField  *passwordTextField;
@property (strong, nonatomic) IBOutlet UITextField  *confirmPasswordTextField;

- (BOOL)formIsValid;

@end
