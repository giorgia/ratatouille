//
//  RTTLLSignUpViewController.m
//  Ratatouille
//
//  Created by Giorgia Marenda on 11/23/13.
//  Copyright (c) 2013 Giorgia Marenda. All rights reserved.
//

#import "RTTLLSignUpViewController.h"
#import "UIImageView+AFNetworking.h"
#import "RTTLLHTTPSessionManager.h"
#import "UIImage+Additions.h"
#import "RTTLLAppDelegate.h"
#import "PSPDFAlertView.h"
#import "NSString+MD5.h"

@interface RTTLLSignUpViewController ()<UINavigationControllerDelegate, UIImagePickerControllerDelegate,UIActionSheetDelegate>

@property (strong, nonatomic) IBOutlet UILabel              *titleLabel;
@property (strong, nonatomic) IBOutlet FBSDKProfilePictureView *avatarImageView;
@property (strong, nonatomic) IBOutlet UIButton             *avatarButton;

@property (strong, nonatomic) IBOutlet UILabel              *privacyLabel;

@property (strong, nonatomic) IBOutlet UIButton             *facebookButton;
@property (strong, nonatomic) IBOutlet UIButton             *signUpButton;

@property (nonatomic) UIImage                               *avatarImage;
@property (nonatomic) UIImagePickerController               *imagePickerController;

@end

@implementation RTTLLSignUpViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.titleLabel.font                = LATO_REG(13);
    self.facebookButton.titleLabel.font = LATO_REG(13);
    self.signUpButton.titleLabel.font   = LATO_REG(13);
    self.privacyLabel.font              = LATO_REG(12);
   
    self.avatarButton.layer.cornerRadius    = self.avatarButton.frame.size.height / 2;
    
    self.avatarImageView.hidden = YES;
}

- (void)onProfileUpdated:(NSNotification*)notification {

    
}


- (IBAction)fillWithFacebookTapped:(id)sender
{
    FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc] init];
    [loginManager logInWithReadPermissions:@[@"public_profile", @"email"] fromViewController:self  handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        [SVProgressHUD dismiss];
        [FBSDKProfile enableUpdatesOnAccessTokenChange:YES];
        
        if (!error) {
            [FBSDKProfile loadCurrentProfileWithCompletion:^(FBSDKProfile *profile, NSError *error) {
                self.nameTextField.text         = profile.firstName;
                self.surnameTextField.text      = profile.lastName;
                self.avatarImageView.hidden     = NO;
                self.avatarImageView.profileID  = profile.userID;
                [self.avatarButton setBackgroundImage: [[UIImage alloc] init] forState:UIControlStateNormal];
                self.avatarImageView.layer.cornerRadius = self.avatarButton.frame.size.height / 2;
                [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters: @{@"fields": @"email"}] startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                    if (result != nil && result[@"email"] != nil) {
                        self.emailTextField.text = result[@"email"];
                    }
                 }];
            }];
        }
    }];
    
}

- (IBAction)getPhoto:(id)sender
{
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Avatar from"
                                                             delegate:self
                                                    cancelButtonTitle:@"Cancel"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"Camera", @"Library", nil];
    actionSheet.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
    [actionSheet showInView:self.view];
}

- (BOOL)formIsValid {
    
    if (!self.emailTextField.text.length    ||
        !self.passwordTextField.text.length ||
        !self.nameTextField.text.length     ||
        !self.surnameTextField.text.length)
    {
        [SVProgressHUD showErrorWithStatus:@"Fill all required fields, please."];
        return NO;
    }
    
    if(self.passwordTextField.text.length && ![self.passwordTextField.text isEqualToString:self.confirmPasswordTextField.text])
    {
        [SVProgressHUD showErrorWithStatus:@"Password and confirm password must be equals."];
        return NO;
    }
    
    return YES;
}

- (IBAction)signUpTapped:(id)sender
{
    if ([self formIsValid]) {
        NSString *message = [NSString stringWithFormat:@"Hi %@,\nwith Ratatouille, people around you will be able to see your fridge location, and could contact in case they are interested in your shared items.\nFor this purpose this app needs to share some private informations (as name, surname, email, phone number (optional) and your fridge position). \n\nDon't esitate, help us to heal the world!", self.nameTextField.text];
        PSPDFAlertView *alert = [[PSPDFAlertView alloc] initWithTitle:@"" message:message];
        [alert addButtonWithTitle:@"Cancel"];
        
        [alert addButtonWithTitle:@"Ok" block:^{
            [SVProgressHUD show];
            self.avatarImage = self.avatarImage ? self.avatarImage : [self getImageProfilePicture];
            [self postSignUp];
        }];
        
        [alert showWithTintColor:COLOR_SMERALD];
    }
}

- (void)postSignUp
{
    NSDictionary *info = @{ @"email":       self.emailTextField.text,
                            @"password":    self.passwordTextField.text,
                            @"name":        self.nameTextField.text,
                            @"surname":     self.surnameTextField.text,
                            @"mobile":      self.phoneNumber.text,
                            };
    
    [SVProgressHUD showWithStatus:@"Signing up..." maskType:SVProgressHUDMaskTypeGradient];
    
    UIImage *scaledImage = [self.avatarImage resizedImage:CGSizeMake(200, 200) drawTransposed:YES interpolationQuality:kCGInterpolationMedium];
    
    [[RTTLLHTTPSessionManager sharedManager] signUpWithInfo:info andAvatar:scaledImage  completion:^(NSError *error)
     {
         if (error) {
             [SVProgressHUD showErrorWithStatus:[error errorDescription]];
         } else {
             [SVProgressHUD dismiss];
             [self performSegueWithIdentifier:@"fridgeLocalize" sender:self];
         }
     }];
}

- (UIImage*)getImageProfilePicture
{
    UIImage *image = nil;
    
    for (NSObject *obj in [self.avatarImageView subviews]) {
        if ([obj isMemberOfClass:[UIImageView class]]) {
            UIImageView *objImg = (UIImageView *)obj;
            image = objImg.image;
            break;
        }
    }
    return image;
}


#pragma mark -
#pragma mark UIImagePickerController

- (void)showImagePickerForSourceType:(UIImagePickerControllerSourceType)sourceType
{
    UIImagePickerController *imagePickerController      = [[UIImagePickerController alloc] init];
    imagePickerController.modalPresentationStyle        = UIModalPresentationCurrentContext;
    imagePickerController.allowsEditing                 = YES;
    imagePickerController.sourceType                    = sourceType;
    imagePickerController.delegate                      = self;
    imagePickerController.navigationBar.barTintColor    = COLOR_TORTORA;

    self.imagePickerController = imagePickerController;
    [self presentViewController:self.imagePickerController animated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [picker dismissViewControllerAnimated:YES completion:NULL];
    picker.allowsEditing            = YES;
    picker.modalPresentationStyle   = UIModalPresentationCurrentContext;
    
    UIImage *chosenImage            = info[UIImagePickerControllerEditedImage];
    self.avatarImageView.hidden     = YES;
    self.avatarImage                = chosenImage;
    [self.avatarButton setBackgroundImage:chosenImage forState:UIControlStateNormal];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark -
#pragma mark UIActionSheetDelegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch(buttonIndex)
    {
        case 0:
        {
            if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
                
                [self showImagePickerForSourceType:UIImagePickerControllerSourceTypeCamera];
            }
        }
            break;
        case 1:
        {
            [self showImagePickerForSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
        }
        default:
            break;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
