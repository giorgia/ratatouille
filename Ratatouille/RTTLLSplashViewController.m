//
//  RTTLLSplashViewController.m
//  Ratatouille
//
//  Created by Giorgia Marenda on 11/23/13.
//  Copyright (c) 2013 Giorgia Marenda. All rights reserved.
//

#import "RTTLLSplashViewController.h"
#import "RTTLLLoginViewController.h"
#import "RTTLLMagnetBehavior.h"

@interface RTTLLSplashViewController ()

@property (strong, nonatomic) IBOutlet UIButton     *getStartedButton;
@property (strong, nonatomic) IBOutlet UIView       *magneticView;
@property (strong, nonatomic) RTTLLMagnetBehavior   *magnetBehavior;
@property (strong, nonatomic) IBOutlet UIImageView  *RItem;
@property (strong, nonatomic) IBOutlet UIImageView  *TItem;
@property (strong, nonatomic) IBOutlet UIImageView  *T2Item;
@property (strong, nonatomic) IBOutlet UIImageView  *LItem;
@property (strong, nonatomic) IBOutlet UIImageView  *L2Item;
@property (strong, nonatomic) UIDynamicAnimator     *animator;
@property (strong, nonatomic) IBOutlet UIView       *buttonView;
@property (strong, nonatomic) IBOutlet UIImageView  *backgroundImageView;

@end

@implementation RTTLLSplashViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (UIDynamicAnimator*)animator
{
    if (!_animator) {
        _animator = [[UIDynamicAnimator alloc] initWithReferenceView:self.magneticView];
    }
    return _animator;
}

- (RTTLLMagnetBehavior*)magnetBehavior
{
    if (!_magnetBehavior) {
        _magnetBehavior = [[RTTLLMagnetBehavior alloc] init];
        
        [self.animator addBehavior:_magnetBehavior];
    }
    return _magnetBehavior;
}

- (IBAction)pan:(UIPanGestureRecognizer *)sender {
    
    if (sender.state == UIGestureRecognizerStateChanged) {
        
        CGPoint translation = [sender translationInView:self.magneticView];
        sender.view.center  = CGPointMake(sender.view.center.x + translation.x , sender.view.center.y+ translation.y);
        [sender setTranslation:CGPointZero inView:self.magneticView];
        [self.animator updateItemUsingCurrentState:sender.view];
    }
}

- (void)showButtonView
{
    CGFloat screenHeight = [[UIScreen mainScreen] bounds].size.height;
    [UIView animateWithDuration:0.3 animations:^{

        self.buttonView.frame = CGRectMake(0.0, screenHeight - self.buttonView.frame.size.height, self.buttonView.frame.size.width, self.buttonView.frame.size.height);
    }];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.RItem.transform    =  CGAffineTransformMakeRotation(-M_PI/6);
    self.TItem.transform    =  CGAffineTransformMakeRotation(M_PI/7);
    self.T2Item.transform   =  CGAffineTransformMakeRotation(-M_PI/8);
    self.L2Item.transform   =  CGAffineTransformMakeRotation(-M_PI/12);
    
    
    [self.magnetBehavior addItem:self.RItem];
    [self.magnetBehavior addItem:self.TItem];
    [self.magnetBehavior addItem:self.T2Item];
    [self.magnetBehavior addItem:self.LItem];
    [self.magnetBehavior addItem:self.L2Item];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.getStartedButton.titleLabel.font = LATO_REG(13);
    
    [self performSelector:@selector(showButtonView) withObject:nil afterDelay:1];
    
    [[self navigationController] setNavigationBarHidden:YES animated:NO];
}

- (IBAction)getStarted:(id)sender
{
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"tutorial_done"]) {
        [self performSegueWithIdentifier:@"loginSegue" sender:self];
    } else {
        [self performSegueWithIdentifier:@"tutorialSegue" sender:self];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
