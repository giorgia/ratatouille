//
//  RTTLLStats.h
//  Ratatouille
//
//  Created by Giorgia Marenda on 3/22/14.
//  Copyright (c) 2014 Giorgia Marenda. All rights reserved.
//

#import "JSONModel.h"

@interface RTTLLStats : JSONModel

@property (assign, nonatomic) int   users;
@property (assign, nonatomic) int   wasted;
@property (assign, nonatomic) int   shared;
@property (assign, nonatomic) int   given;

@end
