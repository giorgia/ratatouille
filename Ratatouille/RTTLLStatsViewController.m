//
//  RTTLLStatsViewController.m
//  Ratatouille
//
//  Created by Giorgia Marenda on 2/13/14.
//  Copyright (c) 2014 Giorgia Marenda. All rights reserved.
//
#import <XYPieChart/XYPieChart.h>
#import "RTTLLStatsViewController.h"
#import "RTTLLHTTPSessionManager.h"
#import "RTTLLShareManager.h"
#import "RTTLLStats.h"

@interface RTTLLStatsViewController ()<XYPieChartDataSource, XYPieChartDelegate>

@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray   *titlesLabels;
@property (strong, nonatomic) IBOutlet UILabel                      *usersLabel;
@property (strong, nonatomic) IBOutlet UILabel                      *shareLabel;
@property (strong, nonatomic) IBOutlet XYPieChart                   *pieChart;
@property (strong, nonatomic) IBOutlet UIButton                     *aboutButton;

@property (strong, nonatomic) NSTimer                               *timer;
@property (assign, nonatomic) NSUInteger                            fromValue;
@property (strong, nonatomic) RTTLLStats                            *stats;

@end

@implementation RTTLLStatsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    for (UILabel *title in self.titlesLabels)
    {
        title.font = LATO_REG(11);
    }
    
    self.usersLabel.font = CASSANNET_BOLD(20);
    self.shareLabel.font = LATO_REG(9);
    self.aboutButton.titleLabel.font = LATO_REG(11);

    [self addPieChart];
}

- (void)addPieChart
{
    [self.pieChart setDataSource:self];
    [self.pieChart setDelegate:self];
    [self.pieChart setStartPieAngle:M_PI_2];
    [self.pieChart setAnimationSpeed:1.0];
    [self.pieChart setLabelFont:LATO_REG(9)];	//optional
    [self.pieChart setLabelColor:[UIColor whiteColor]];	//optional, defaults to white
    [self.pieChart setShowPercentage:NO];	//optional
    [self.pieChart setUserInteractionEnabled:NO];
    [self.pieChart setPieBackgroundColor:COLOR_BEIGE];	//optional
    [self.pieChart reloadData];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.timer = [NSTimer scheduledTimerWithTimeInterval:120 target:self selector:@selector(loadStats) userInfo:nil repeats:YES];

    [self loadStats];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.timer invalidate];
    self.timer = nil;
    self.stats = nil;
    [self.pieChart reloadData];
}

- (void)loadStats
{
    __block typeof(self) bself = self;
    [[RTTLLHTTPSessionManager sharedManager] statsCompletion:^(RTTLLStats *stats, NSError *error) {
        if (error) {
            [SVProgressHUD showErrorWithStatus:[error errorDescription]];
            return;
        }
        bself.stats = stats;
        [self drawStats];
    }];
}

- (void)drawStats
{
    self.usersLabel.text = [NSString stringWithFormat:@"%d", self.stats.users];
    [self.pieChart reloadData];
}

-(void)addUpTimer
{
    NSInteger initValue = [self.usersLabel.text integerValue] + 1;
    
    self.usersLabel.text = [NSString stringWithFormat:@"%@", @(initValue)];
    
    if(initValue >= 12000) {
        [self.timer invalidate];
        self.usersLabel.text = [NSString stringWithFormat:@"%d", 12000];
    }
}

#pragma -
#pragma mark XYPieChart DataSource

- (NSUInteger)numberOfSlicesInPieChart:(XYPieChart *)pieChart
{
    return 3;
}

- (CGFloat)pieChart:(XYPieChart *)pieChart valueForSliceAtIndex:(NSUInteger)index
{
    switch (index) {
        case 0:
            return self.stats.wasted;
            break;
        case 1:
            return self.stats.given;
            break;
        case 2:
            return self.stats.shared;
            break;
            
        default:
            break;
    }
    return 0;
}

-(NSString *)pieChart:(XYPieChart *)pieChart textForSliceAtIndex:(NSUInteger)index
{
    switch (index) {
        case 0:
            return @"WASTED";
            break;
        case 1:
            return @"GIVEN";
            break;
        case 2:
            return @"SHARED";
            break;
            
        default:
            break;
    }
    return @"";
}

-(UIColor *)pieChart:(XYPieChart *)pieChart colorForSliceAtIndex:(NSUInteger)index
{
    switch (index) {
        case 0:
            return COLOR_LOBSTER;
            break;
        case 1:
            return COLOR_ACID;
            break;
        case 2:
            return COLOR_OLIVE;
            break;
            
        default:
            break;
    }
    return nil;
}

- (IBAction)onShare:(id)sender
{
    NSString *text = SHARE_TEXT;
    UIImage *image = [UIImage imageNamed:@"icon"];
    NSURL *url = [NSURL URLWithString:APP_STORE_LINK];
    [RTTLLShareManager shareActionFrom:self withText:text image:image url:url];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
