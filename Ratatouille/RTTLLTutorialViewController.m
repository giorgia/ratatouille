//
//  RTTLLTutorialViewController.m
//  Ratatouille
//
//  Created by Giorgia Marenda on 3/15/14.
//  Copyright (c) 2014 Giorgia Marenda. All rights reserved.
//

#import "RTTLLTutorialViewController.h"
#import "EAIntroView.h"

@interface RTTLLTutorialViewController ()<EAIntroDelegate>
@property (strong, nonatomic) IBOutlet EAIntroView *introView;

@end

@implementation RTTLLTutorialViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    // 1
    EAIntroPage *page1  = [EAIntroPage page];
    page1.title         = @"HI THERE!";
    page1.desc          = @"Your fridge is still bursting with food?\nToo much leftover from last night party?";
    page1.titleImage    = [UIImage imageNamed:@"tutorial-1"];
    [self configurePage:page1];
    
    // 2
    EAIntroPage *page2  = [EAIntroPage page];
    page2.title         = @"SHARE";
    page2.desc          = @"Offer your extra food in a snap.";
    page2.titleImage    = [UIImage imageNamed:@"tutorial-2"];
    [self configurePage:page2];
    
    // 3
    EAIntroPage *page3  = [EAIntroPage page];
    page3.title         = @"SEARCH";
    page3.desc          = @"Search around you the missing\ningredient to complete your recipe.";
    page3.titleImage    = [UIImage imageNamed:@"tutorial-3"];
    [self configurePage:page3];
    
    // 4
    EAIntroPage *page4  = [EAIntroPage page];
    page4.title         = @"MEET UP";
    page4.desc          = @"With Ratatouille, you’ll find cool people\nin your neighborhood just like you.";
    page4.titleImage    = [UIImage imageNamed:@"tutorial-4"];
    [self configurePage:page4];
 
    // 5
    
    EAIntroPage *page5  = [EAIntroPage page];
    page5.title         = @"START NOW";
    page5.desc          = @"Swipe left and right to navigate.";
    [self configurePage:page5];
    
    CGFloat positionY   = [UIScreen mainScreen].bounds.size.height/2 - 298 + 70;
    UIImageView *image5 = [[UIImageView alloc] initWithFrame:CGRectMake(2.0, positionY, 316, 298)];
    NSMutableArray *arr = [NSMutableArray arrayWithCapacity:17];

    for(int i = 0; i < 17; i++) {
        [arr addObject:[NSNull null]];
    }
    for (int i = 0; i < 9 ; i++) {
        UIImage *image = [UIImage imageNamed:[NSString stringWithFormat:@"tutorial-5-%d",i+1]];
        [arr replaceObjectAtIndex:i withObject:image];
        [arr replaceObjectAtIndex:16-i withObject:image];
    }
    
    image5.animationImages = arr;
    image5.animationDuration = 2.0;
    [image5 startAnimating];

    page5.subviews = @[image5];

    // custom view from nib
    self.introView.backgroundColor                              = COLOR_LIGHT_BEIGE;
    self.introView.pageControl.pageIndicatorTintColor           = COLOR_BEIGE;
    self.introView.pageControl.currentPageIndicatorTintColor    = COLOR_LOBSTER;
    self.introView.pageControlY                                 = 90.0;
    self.introView.skipButton.titleLabel.font                   = LATO_REG(14);
    [self.introView.skipButton setTitleColor:COLOR_LOBSTER forState:UIControlStateNormal];
    [self.introView.skipButton setTitle:@"SKIP" forState:UIControlStateNormal];
    [self.introView.skipButton setFrame:CGRectMake((320-230)/2, [UIScreen mainScreen].bounds.size.height - 60, 230, 40)];

    [self.introView setPages:@[page1, page2, page3, page4, page5]];
    [self.introView setDelegate:self];
}

- (void)configurePage:(EAIntroPage*)page
{
    page.titleFont = CASSANNET_BOLD(20);
    page.descFont = LATO_LIGHT(16);
    page.titlePositionY = 170;
    page.descPositionY = 150;
    
    page.imgPositionY = [UIScreen mainScreen].bounds.size.height/2 - page.titleImage.size.height + 70;
    page.titleColor = page.descColor = COLOR_BROWN;

}

-(void)introDidFinish:(EAIntroView *)introView
{
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"tutorial_done"]) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"tutorial_done"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [self performSegueWithIdentifier:@"loginSegue" sender:self];
    } else {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
