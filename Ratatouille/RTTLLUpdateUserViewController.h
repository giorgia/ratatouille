//
//  RTTLLUpdateUserViewController.h
//  Ratatouille
//
//  Created by Giorgia Marenda on 2/8/14.
//  Copyright (c) 2014 Giorgia Marenda. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RTTLLUser;

@interface RTTLLUpdateUserViewController : UITableViewController

@property (strong, nonatomic) RTTLLUser *user;

@end
