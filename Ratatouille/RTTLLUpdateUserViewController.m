//
//  RTTLLUpdateUserViewController.m
//  Ratatouille
//
//  Created by Giorgia Marenda on 2/8/14.
//  Copyright (c) 2014 Giorgia Marenda. All rights reserved.
//

#import "RTTLLUpdateUserViewController.h"
#import "UIImageView+AFNetworking.h"
#import "RTTLLHTTPSessionManager.h"
#import "UIImage+Additions.h"
#import "RTTLLAppDelegate.h"
#import "RTTLLUser.h"

@interface RTTLLUpdateUserViewController ()<UINavigationControllerDelegate, UIImagePickerControllerDelegate,UIActionSheetDelegate>

@property (strong, nonatomic) IBOutlet UILabel      *titleLabel;
@property (strong, nonatomic) IBOutlet UIImageView  *avatarImageView;
@property (strong, nonatomic) IBOutlet UITextField  *emailTextField;
@property (strong, nonatomic) IBOutlet UITextField  *nameTextField;
@property (strong, nonatomic) IBOutlet UITextField  *surnameTextField;
@property (strong, nonatomic) IBOutlet UITextField  *phoneTextField;
@property (strong, nonatomic) IBOutlet UIButton     *updateButton;
@property (strong, nonatomic) IBOutlet UIButton     *deleteButton;

@property (nonatomic) UIImage                       *avatarImage;
@property (nonatomic) UIImagePickerController       *imagePickerController;

- (IBAction)updateUser:(id)sender;
- (IBAction)getPhoto:(UITapGestureRecognizer*)gesture;

@end

@implementation RTTLLUpdateUserViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.titleLabel.font                    = LATO_REG(13);
    
    self.avatarImageView.layer.cornerRadius = self.avatarImageView.frame.size.height / 2;
    self.updateButton.titleLabel.font       = LATO_REG(13);
    self.deleteButton.titleLabel.font       = LATO_REG(11);

    self.emailTextField.text    = self.user.email;
    self.nameTextField.text     = self.user.name;
    self.surnameTextField.text  = self.user.surname;
    self.phoneTextField.text    = self.user.mobile;
    
    [self.avatarImageView setImageWithURL:[NSURL URLWithString:self.user.image_url] placeholderImage:[UIImage imageNamed:@"insert_img_profile"]];
}

- (IBAction)updateUser:(id)sender
{
    if ([self formIsValid]) {
        NSDictionary *info = @{ @"email":   self.emailTextField.text,
                                @"name":    self.nameTextField.text,
                                @"surname": self.surnameTextField.text,
                                @"mobile":  self.phoneTextField.text,
                                };
        
        [SVProgressHUD showWithStatus:@"Updating..." maskType:SVProgressHUDMaskTypeGradient];
        
        UIImage *scaledImage = [self.avatarImage resizedImage:CGSizeMake(200, 200) drawTransposed:YES interpolationQuality:kCGInterpolationMedium];
        
        [[RTTLLHTTPSessionManager sharedManager] updateMeWithInfo:info andAvatar:scaledImage completion:^(NSError *error)
         {
             [SVProgressHUD dismiss];
             if (error) {
                 [SVProgressHUD showErrorWithStatus:[error errorDescription]];
             }
             [[NSNotificationCenter defaultCenter] postNotificationName:@"UserUpdatedNotification" object:self];

             [self dismissViewControllerAnimated:YES completion:nil];
         }];
    }
}

- (IBAction)deleteUser:(id)sender
{
    NSString *message = @"Are you sure do you want to delete your account? \nYou will lose all your data.";
    PSPDFAlertView *alert = [[PSPDFAlertView alloc] initWithTitle:@"Delete account"
                                                          message:message];
    [alert addButtonWithTitle:@"Cancel"];
    [alert addButtonWithTitle:@"Yes" block:^{
        [SVProgressHUD show];
        [[RTTLLHTTPSessionManager sharedManager] deleteMeCompletion:^(NSError *error) {
            RTTLLAppDelegate *delegate = (RTTLLAppDelegate *)[[UIApplication sharedApplication] delegate];
            [SVProgressHUD dismiss];
            [delegate loadStoryboardAnimated:YES];
        }];
    }];
    [alert showWithTintColor:COLOR_SMERALD];
    
    
}

- (IBAction)getPhoto:(UITapGestureRecognizer*)gesture
{
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Avatar from"
                                                             delegate:self
                                                    cancelButtonTitle:@"Cancel"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"Camera", @"Library", nil];
    actionSheet.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
    [actionSheet showInView:self.view];
}

- (BOOL)formIsValid {
    
    if (!self.emailTextField.text.length ||
        !self.nameTextField.text.length ||
        !self.surnameTextField.text.length)
    {
        [SVProgressHUD showErrorWithStatus:@"Fill all required fields, please."];
        return NO;
    }
    return YES;
}

#pragma mark -
#pragma mark UIImagePickerController

- (void)showImagePickerForSourceType:(UIImagePickerControllerSourceType)sourceType
{
    UIImagePickerController *imagePickerController  = [[UIImagePickerController alloc] init];
    imagePickerController.modalPresentationStyle    = UIModalPresentationCurrentContext;
    imagePickerController.allowsEditing             = YES;
    imagePickerController.sourceType                = sourceType;
    imagePickerController.delegate                  = self;
    imagePickerController.navigationBar.barTintColor = COLOR_TORTORA;

    self.imagePickerController = imagePickerController;
    [self presentViewController:self.imagePickerController animated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [picker dismissViewControllerAnimated:YES completion:NULL];
    picker.allowsEditing = YES;
    picker.modalPresentationStyle = UIModalPresentationCurrentContext;
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    self.avatarImage = chosenImage;
    self.avatarImageView.image = chosenImage;
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark -
#pragma mark UIActionSheetDelegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    switch(buttonIndex)
    {
        case 0:
        {
            if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
                
                [self showImagePickerForSourceType:UIImagePickerControllerSourceTypeCamera];
            }
        }
            break;
        case 1:
        {
            [self showImagePickerForSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
        }
        default:
            break;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
