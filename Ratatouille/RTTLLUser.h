//
//  RTTLLUser.h
//  Ratatouille
//
//  Created by Giorgia Marenda on 11/16/13.
//  Copyright (c) 2013 Giorgia Marenda. All rights reserved.
//

#import "JSONModel.h"

#define AUTH_TOKEN_KEY          @"authentication_token"
#define AUTH_USER_EMAIL         @"authentication_email"
#define POSITION_CONFIRM_KEY    @"position_confirmed"


#define USER_EMAIL              @"email"

#define USERS_KEY               @"users"
#define USER_KEY                @"user"
#define ITEM_KEY                @"item"

@class RTTLLFridge;

@protocol RTTLLUser @end

@interface RTTLLUser : JSONModel

@property (assign, nonatomic) int                         id;
@property (strong, nonatomic) NSString                    *email;
@property (strong, nonatomic) NSString<Optional>          *name;
@property (strong, nonatomic) NSString<Optional>          *surname;
@property (strong, nonatomic) NSString<Optional>          *mobile;
@property (strong, nonatomic) NSString<Optional>          *image_url;
@property (strong, nonatomic) RTTLLFridge<Optional>       *fridge;


- (BOOL)isMe;
+ (void)setMyUserId:(NSInteger)myUserId;
+ (NSInteger)myUserId;

@end
