//
//  RTTLLUser.m
//  Ratatouille
//
//  Created by Giorgia Marenda on 11/16/13.
//  Copyright (c) 2013 Giorgia Marenda. All rights reserved.
//

#import "RTTLLUser.h"

@implementation RTTLLUser

- (BOOL)isMe
{
    NSInteger myUserId = [[self class] myUserId];
    return self.id == myUserId;
}

+ (void)setMyUserId:(NSInteger)myUserId
{
    [[NSUserDefaults standardUserDefaults] setInteger:myUserId forKey:@"my_user_id"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSInteger)myUserId
{
    return [[NSUserDefaults standardUserDefaults] integerForKey:@"my_user_id"];
}

@end
