//
//  RTTLLViewController.h
//  Ratatouille
//
//  Created by Giorgia Marenda on 10/31/13.
//  Copyright (c) 2013 Giorgia Marenda. All rights reserved.
//

#import <UIKit/UIKit.h>

#define ShowStatsNotification @"showStatsNotification"
#define ShowProfileNotification @"showProfileNotification"

@interface RTTLLViewController : UIViewController

@end
