//
//  RTTLLViewController.m
//  Ratatouille
//
//  Created by Giorgia Marenda on 10/31/13.
//  Copyright (c) 2013 Giorgia Marenda. All rights reserved.
//

#import "RTTLLViewController.h"

#define SegueIdentifierFirst @"embedFirst"
#define SegueIdentifierSecond @"embedSecond"

@interface RTTLLViewController () <UIScrollViewDelegate>
@property (strong, nonatomic) IBOutlet UIScrollView                 *scrollView;
@property (strong, nonatomic) IBOutletCollection(UIView) NSArray    *controllers;

@end

@implementation RTTLLViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    [NSNotificationCenter.defaultCenter addObserver: self selector: @selector(showStats) name: ShowStatsNotification object: nil];
    [NSNotificationCenter.defaultCenter addObserver: self selector: @selector(showProfile) name: ShowProfileNotification object: nil];
}

- (void)showStats {
    [self scrollToPage: 0];
}

- (void)showProfile {
    [self scrollToPage: 2];
}

- (void)scrollToPage:(NSInteger)page {
    if (page < self.controllers.count) {
//        UIViewController* controller = [[self.childViewControllers[page] viewControllers] lastObject];
//        [self.scrollView scrollRectToVisible:controller.view.frame animated:true];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    static NSInteger previousPage   = 0;
    CGFloat pageWidth               = scrollView.frame.size.width;
    float fractionalPage            = scrollView.contentOffset.x / pageWidth;
    NSInteger page                  = lround(fractionalPage) + 1;
    
    if (previousPage != page) {
        
        id controller       = [[self.childViewControllers[page] viewControllers] lastObject];
        [controller viewWillAppear:YES];
        id prevController   = [[self.childViewControllers[previousPage] viewControllers] lastObject];
        [prevController viewWillDisappear:YES];
        previousPage        = page;
    }
}

@end
