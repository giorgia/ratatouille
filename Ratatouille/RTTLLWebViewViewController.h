//
//  RTTLLWebViewViewController.h
//  Ratatouille
//
//  Created by Giorgia Marenda on 3/10/14.
//  Copyright (c) 2014 Giorgia Marenda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RTTLLWebViewViewController : UIViewController

@property (strong, nonatomic) NSString* urlString;

@end
