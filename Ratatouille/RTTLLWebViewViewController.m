//
//  RTTLLWebViewViewController.m
//  Ratatouille
//
//  Created by Giorgia Marenda on 3/10/14.
//  Copyright (c) 2014 Giorgia Marenda. All rights reserved.
//

#import "RTTLLWebViewViewController.h"
#import "RTTLLHTTPSessionManager.h"

@interface RTTLLWebViewViewController ()

@property (strong, nonatomic) IBOutlet UIWebView *webView;

@end

@implementation RTTLLWebViewViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self loadUrl];
}

- (void)loadUrl
{
    [SVProgressHUD showWithStatus:@"Loading..."];
    [[RTTLLHTTPSessionManager sharedManager] contentFromURL:[NSURL URLWithString:self.urlString] complentionBlock:^(NSString *content, NSError *error) {
        [SVProgressHUD dismiss];
        if (error) {
            [SVProgressHUD showErrorWithStatus:error.localizedDescription];
            return;
        }
        [self.webView loadHTMLString:content baseURL:[NSURL URLWithString:BASE_URL]];
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
