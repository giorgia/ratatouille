//
//  UIImage+Additions.h
//  Ratatouille
//
//  Created by Giorgia Marenda on 2/1/14.
//  Copyright (c) 2014 Giorgia Marenda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Additions)

- (UIImage *)resizedImage:(CGSize)newSize
           drawTransposed:(BOOL)transpose
     interpolationQuality:(CGInterpolationQuality)quality;

@end
