//
//  UIView+Additions.h
//  Ratatouille
//
//  Created by Giorgia Marenda on 2/2/14.
//  Copyright (c) 2014 Giorgia Marenda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Additions)

- (CALayer*)bottomBorderOfWidth:(CGFloat)width andColor:(UIColor*)color;
- (void)topBorderOfWidth:(CGFloat)width andColor:(UIColor*)color;

@end
