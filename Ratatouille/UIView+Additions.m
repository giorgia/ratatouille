//
//  UIView+Additions.m
//  Ratatouille
//
//  Created by Giorgia Marenda on 2/2/14.
//  Copyright (c) 2014 Giorgia Marenda. All rights reserved.
//

#import "UIView+Additions.h"

@implementation UIView (Additions)

- (CALayer*)layerWithBorderOfWidth:(CGFloat)width andColor:(UIColor*)color
{
    CALayer *border = [CALayer layer];
    border.borderColor = color.CGColor;
    border.borderWidth = width;
    
    return border;
}

- (CALayer*)bottomBorderOfWidth:(CGFloat)width andColor:(UIColor*)color
{
    CALayer *border = [self layerWithBorderOfWidth:width andColor:color];
    border.frame = CGRectMake(0.0, CGRectGetHeight(self.frame) - width, CGRectGetWidth(self.frame), width);
    [self.layer addSublayer:border];
    return border;
}

- (void)topBorderOfWidth:(CGFloat)width andColor:(UIColor*)color
{
    CALayer *border = [self layerWithBorderOfWidth:width andColor:color];
    border.frame = CGRectMake(0.0, 0.0, CGRectGetWidth(self.frame), width);
    [self.layer addSublayer:border];
}

@end
