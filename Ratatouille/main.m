    
//
//  main.m
//  Ratatouille
//
//  Created by Giorgia Marenda on 10/31/13.
//  Copyright (c) 2013 Giorgia Marenda. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "RTTLLAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([RTTLLAppDelegate class]));
    }
}
