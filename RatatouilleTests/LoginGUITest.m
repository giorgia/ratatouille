//
//  LoginGUITest.m
//  Ratatouille
//
//  Created by Giorgia Marenda on 1/21/14.
//  Copyright (c) 2014 Giorgia Marenda. All rights reserved.
//

#import "Kiwi.h"
#import "RTTLLSignUpViewController.h"

SPEC_BEGIN(RTTLLLoginSpec)

describe(@"Sign Up View Controller", ^{
    
    UIStoryboard *loginStoryboard = [UIStoryboard storyboardWithName:@"Login" bundle:nil];
    RTTLLSignUpViewController *signUpViewController = [loginStoryboard instantiateViewControllerWithIdentifier:@"SignUp"];
    [signUpViewController loadView];
   
    context(@"When mandatory fields are filled", ^{
        beforeEach(^{
            signUpViewController.nameTextField.text     = @"Giorgia";
            signUpViewController.surnameTextField.text  = @"Marenda";
            signUpViewController.emailTextField.text    = @"Marenda";
            signUpViewController.phoneNumber.text       = @"Marenda";
            signUpViewController.passwordTextField.text = @"Marenda";
            signUpViewController.confirmPasswordTextField.text = @"Marenda";
        });
        
        it(@"success check validation ", ^{
            [[theValue([signUpViewController formIsValid]) should] beTrue];
        });
        
        context(@"with different passwords", ^{
            beforeEach(^{
                signUpViewController.confirmPasswordTextField.text = @"asfdasd";
            });
           
            it(@"fails", ^{
                [[theValue([signUpViewController formIsValid]) should] beFalse];
            });

        });
    });
    
});

SPEC_END