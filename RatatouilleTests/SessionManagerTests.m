//
//  SessionManagerTests.m
//  RatatouilleTests
//
//  Created by Giorgia Marenda on 10/31/13.
//  Copyright (c) 2013 Giorgia Marenda. All rights reserved.
//

#import "Kiwi.h"
#import "RTTLLUser.h"
#import "RTTLLFridge.h"
#import "RTTLLHTTPSessionManager.h"
#import "NSError+Additions.h"

SPEC_BEGIN(RTTLLHTTPSessionManagerSpec)



describe(@"HTTP session manager", ^{
    
//========================= AUTHENTICATION =======================================

    //SIGN IN
    context(@"Login with existing information", ^{
        
        __block NSString *signInResponse = nil;
        
        NSDictionary *parameters = @{@"email": @"giorgia@gmail.com",
                                     @"password" : @"asdasd"};
        
        [[RTTLLHTTPSessionManager sharedManager] signInWithInfo:parameters completion:^(NSError *error)
         {
             signInResponse = [error errorDescription];
         }];
        
        it(@"should receive no error", ^{
            [[expectFutureValue(signInResponse) shouldEventually] beNil];
        });
    });
  
    //SIGN UP
    context(@"Sign up with existing information", ^{
        
        __block NSString *signUpResponse = @"";
        
        NSDictionary *parameters = @{@"email":      @"giorgia@gmail.com",
                                     @"password" :  @"asd",
                                     @"name":       @"Giorgia",
                                     @"surname":    @"Marenda",
                                     @"mobile":     @"123123123",
                                     @"avatar":     @""
                                     };
        
        [[RTTLLHTTPSessionManager sharedManager] signUpWithInfo:parameters andAvatar:nil completion:^(NSError *error)
         {
             signUpResponse = [error errorDescription];
         }];
        
        it(@"should receive error", ^{
            [[expectFutureValue(signUpResponse) shouldEventually] beNonNil];
        });
        
        it(@"should receive errors for mail and mobile already been taken", ^{
            [[expectFutureValue(signUpResponse) shouldEventually] containString:@"Email: has already been taken"];
            [[expectFutureValue(signUpResponse) shouldEventually] containString:@"Mobile: has already been taken"];
        });
        
        it(@"should receive error for password is too short", ^{
            [[expectFutureValue(signUpResponse) shouldEventually] containString:@"Password: is too short (minimum is 5 characters)"];
        });
    });

//========================= USERS =======================================
    
    //GET ME
    context(@"Fetching service me", ^{
        
        __block RTTLLUser *meFetchedData = nil;
        __block NSError *error = nil;
        
        NSDictionary *parameters = @{@"email": @"giorgia@gmail.com",
                                     @"password" : @"asdasd"};
        
        [[RTTLLHTTPSessionManager sharedManager] signInWithInfo:parameters completion:^(NSError *error)
         {
             [[RTTLLHTTPSessionManager sharedManager] meCompletion:^(RTTLLUser *user, NSError *error) {
                 meFetchedData = user;
                 error = error;
             }];
         }];
        
        it(@"should not receive error", ^{
            [[expectFutureValue(error) shouldEventually] beNil];
        });
        
        it(@"should receive an user that corresponds to me", ^{
            [[expectFutureValue(meFetchedData) shouldEventually] beNonNil];
            [[expectFutureValue(meFetchedData.email) shouldEventually] equal:@"giorgia@gmail.com"];
        });
        
    });

    //PUT ME
    context(@"Update mobile of my user", ^{
        
        __block RTTLLUser *upMeFetchedData = nil;
        __block NSError *error = nil;
        
        NSDictionary *parameters = @{@"email": @"giorgia@gmail.com",
                                     @"password" : @"asdasd"};
        
        [[RTTLLHTTPSessionManager sharedManager] signInWithInfo:parameters completion:^(NSError *error)
         {
             NSDictionary *updatedParams = @{@"name": @"Giorgia",
                                             @"surname": @"Marenda",
                                             @"mobile": @"45454545",
                                             @"avatar": @""
                                          };
             [[RTTLLHTTPSessionManager sharedManager] updateMeWithInfo:updatedParams andAvatar:nil completion:^(NSError *error) {
                 error = error;
                 [[RTTLLHTTPSessionManager sharedManager] meCompletion:^(RTTLLUser *user, NSError *error) {
                     upMeFetchedData = user;
                 }];
             }];
         }];
        
        it(@"should not receive error", ^{
            [[expectFutureValue(error) shouldEventually] beNil];
        });
        
        it(@"should return an me user with new mobile", ^{
            [[expectFutureValue(upMeFetchedData) shouldEventually] beNonNil];
            [[expectFutureValue(upMeFetchedData.mobile) shouldEventually] equal:@"45454545"];
        });
    });

    
    //GET USERS NEAR LOCATION
    context(@"Fetching service users near Casier", ^{
        
        __block NSArray *usersFetchedData = nil;
        __block NSError *error = nil;
        
        NSDictionary *parameters = @{@"email": @"giorgia@gmail.com",
                                     @"password" : @"asdasd"};
        
        [[RTTLLHTTPSessionManager sharedManager] signInWithInfo:parameters completion:^(NSError *error)
         {
             [[RTTLLHTTPSessionManager sharedManager] usersNear:CLLocationCoordinate2DMake(45.640159, 12.290451) userLocation:CLLocationCoordinate2DMake(45.640159, 12.290451) radius:@"20000" completion:^(NSArray *results, NSError *error)
              {
                  usersFetchedData = results;
                  error = error;
              }];
         }];
        
        it(@"should not receive error", ^{
            [[expectFutureValue(error) shouldEventually] beNil];
        });
        
        it(@"should receive at least one user", ^{
            [[expectFutureValue(usersFetchedData) shouldEventually] beNonNil];
            [[expectFutureValue(usersFetchedData) shouldEventually] haveAtLeast:1];
        });
        
    });
    
    //GET SINGLE USER
    context(@"Fetching service user with id", ^{
        
        __block RTTLLUser *userFetchedData = nil;
        __block NSError *error = nil;
        
        NSDictionary *parameters = @{@"email": @"giorgia@gmail.com",
                                     @"password" : @"asdasd"};
        
        [[RTTLLHTTPSessionManager sharedManager] signInWithInfo:parameters completion:^(NSError *error)
         {
             [[RTTLLHTTPSessionManager sharedManager] userWithId:10 completion:^(RTTLLUser *user, NSError *error)
             {
                 userFetchedData = user;
                 error = error;
             }];
         }];
        
        it(@"should not receive error", ^{
            [[expectFutureValue(error) shouldEventually] beNil];
        });
        
        it(@"should receive at least one user", ^{
            [[expectFutureValue(userFetchedData) shouldEventually] beNonNil];
            [[expectFutureValue(userFetchedData.email) shouldEventually] equal:@"test@test.com"];
        });
        
    });
    
//========================= FRIDGES =======================================
    
    //PUT MY FRIDGE
    context(@"Update user fridge location", ^{
        
        __block RTTLLUser *fridgeFetchedData = nil;
        __block NSError *error = nil;

        NSDictionary *parameters = @{@"email": @"giorgia@gmail.com",
                                     @"password" : @"asdasd"};
        
        [[RTTLLHTTPSessionManager sharedManager] signInWithInfo:parameters completion:^(NSError *error)
         {
             [[RTTLLHTTPSessionManager sharedManager] updateFridgeLocation:CLLocationCoordinate2DMake(45.640159, 12.290451) completion:^(NSError *error) {
                  error = error;
                 [[RTTLLHTTPSessionManager sharedManager] meCompletion:^(RTTLLUser *user, NSError *error) {
                     fridgeFetchedData = user;
                 }];
              }];
             
             
         }];
        
        it(@"should not receive error", ^{
            [[expectFutureValue(error) shouldEventually] beNil];
        });
        
        it(@"should receive at least one user", ^{
            [[expectFutureValue(fridgeFetchedData) shouldEventually] beNonNil];
            [[expectFutureValue(fridgeFetchedData.fridge.lat) shouldEventually] equal:@"45.640159"];
            [[expectFutureValue(fridgeFetchedData.fridge.lng) shouldEventually] equal:@"12.290451" ];
        });
        
    });

//========================= ITEMS =======================================
    
    //GET ITEMS

    context(@"Fetching items near Casier with search text 'Tomatoes'", ^{
        
        __block NSArray *itemsFetchedData = nil;
        __block NSError *error = nil;
        
        NSDictionary *parameters = @{@"email": @"giorgia@gmail.com",
                                     @"password" : @"asdasd"};
        
        [[RTTLLHTTPSessionManager sharedManager] signInWithInfo:parameters completion:^(NSError *error)
         {
             [[RTTLLHTTPSessionManager sharedManager] itemsNear:CLLocationCoordinate2DMake(45.640159, 12.290451) radius:@"20000" query:@"Tomatoes" completion:^(NSArray *results, NSError *error) {
                 itemsFetchedData = results;
                 error = error;
             }];
         }];
        
        it(@"should not receive error", ^{
            [[expectFutureValue(error) shouldEventually] beNil];
        });
        
        it(@"should receive at least one item", ^{
            [[expectFutureValue(itemsFetchedData) shouldEventually] beNonNil];
            [[expectFutureValue(itemsFetchedData) shouldEventually] haveAtLeast:1];
        });
        
        it(@"should have items that contain string with 'tomatoes'", ^{
            [[expectFutureValue(itemsFetchedData) shouldEventually] beNonNil];
            [[expectFutureValue([itemsFetchedData[0] title]) shouldEventually] containString:@"tomatoes"];
            [[expectFutureValue([itemsFetchedData[0] desc]) shouldEventually] containString:@"tomatoes"];
        });
        
    });
    
    
    //POST ITEM
    context(@"Sign up with existing information", ^{
        
        __block NSString *signUpResponse = @"";
        
        NSDictionary *parameters = @{@"email":      @"giorgia@gmail.com",
                                     @"password" :  @"asd",
                                     @"name":       @"Giorgia",
                                     @"surname":    @"Marenda",
                                     @"mobile":     @"123123123",
                                     @"avatar":     @""
                                     };
        
        [[RTTLLHTTPSessionManager sharedManager] signUpWithInfo:parameters andAvatar:nil completion:^(NSError *error)
         {
             signUpResponse = [error errorDescription];
         }];
        
        it(@"should receive error", ^{
            [[expectFutureValue(signUpResponse) shouldEventually] beNonNil];
        });
        
        it(@"should receive errors for mail and mobile already been taken", ^{
            [[expectFutureValue(signUpResponse) shouldEventually] containString:@"Email: has already been taken"];
            [[expectFutureValue(signUpResponse) shouldEventually] containString:@"Mobile: has already been taken"];
        });
        
        it(@"should receive error for password is too short", ^{
            [[expectFutureValue(signUpResponse) shouldEventually] containString:@"Password: is too short (minimum is 5 characters)"];
        });
    });
    
    //GET ITEM
    context(@"Fetching service item with id", ^{
        
        __block RTTLLItem *itemFetchedData = nil;
        __block NSError *error = nil;
        
        NSDictionary *parameters = @{@"email":      @"giorgia@gmail.com",
                                     @"password" :  @"asdasd"};
        
        [[RTTLLHTTPSessionManager sharedManager] signInWithInfo:parameters completion:^(NSError *error)
         {
             [[RTTLLHTTPSessionManager sharedManager] itemWithId:0 completion:^(RTTLLItem *item, NSError *error)
             {
                  itemFetchedData = item;
                  error = error;
              }];
         }];
        
        it(@"should not receive error", ^{
            [[expectFutureValue(error) shouldEventually] beNil];
        });
        
        it(@"should receive at least one user", ^{
            [[expectFutureValue(itemFetchedData) shouldEventually] beNonNil];
            //[[expectFutureValue(itemFetchedData.title) shouldEventually] equal:@"test item id 0"];
        });
        
    });
    
    //PUT ITEM
    
    //DELETE ITEM
});

SPEC_END
